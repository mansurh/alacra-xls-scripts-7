DECLARE @formid int
DECLARE @uuid varchar(128)


SELECT @formid = 2, @uuid = '${_uuid,s}'

INSERT formsubmits VALUES (@uuid, @formid, '${_ip,s}', GETDATE())

INSERT formstrvars VALUES (@uuid, @formid, 'first', '${first,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'last', '${last,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'company', '${company,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'email', '${email,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'job', '${job,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'otherjob', '${otherjob,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'easy2use', '${easy2use,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'logicallyorganized', '${logicallyorganized,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'reliable', '${reliable,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'searchscreens', '${searchscreens,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'ifindwhatineed', '${ifindwhatineed,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'hasimproved', '${hasimproved,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'customercare', '${Customercare,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'updates', '${updates,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'keywordsearch', '${keywordsearch,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'searchpremium', '${searchpremium,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'dbsearch', '${dbsearch,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'publiccos', '${publiccos,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'privatecos', '${privatecos,s}')

INSERT formtextvars VALUES (@uuid, @formid, 'enhancements', '${enhancements,s}')
INSERT formtextvars VALUES (@uuid, @formid, 'OneChange', '${OneChange,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'Citywatch', '${Citywatch,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Compustat', '${Compustat,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Dealogic', '${Dealogic,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Forrester', '${Forrester,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DBMI', '${DBMI,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DBKBR', '${DBKBR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DBBIR', '${DBBIR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Fitch', '${Fitch,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Mintel', '${Mintel,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'SPRD', '${SPRD,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DEAL', '${DEAL,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Worldscope', '${Worldscope,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'useDBMIorCR', '${useDBMIorCR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'getDBfrom', '${getDBfrom,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'otherdbprovider', '${otherdbprovider,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'MORM', '${MORM,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'iusemorm', '${iusemorm,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'FactSet', '${FactSet,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'AlacraModel', '${AlacraModel,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'heardofAlacrabook', '${heardofAlacrabook,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'wantdemo', '${wantdemo,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'researchhelp', '${researchhelp,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'mktbrokerres', '${mktbrokerres,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'mandares', '${mandares,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'ecores', '${ecores,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'books', '${books,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'comps', '${comps,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'privatecores', '${privatecores,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'creditrisk', '${creditrisk,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'payvia', '${payvia,s}')

INSERT formtextvars VALUES (@uuid, @formid, 'comments', '${comments,s}')

