#!/perl/bin/perl
# Take ISI output from our CGI program and convert to RSS

use XML::Parser;

die "Usage: errorCheck.pl xmlfile\n"
    unless ( @ARGV == 1 );
 
my $xmlfile = $ARGV[0];

# don't handle wildcards
if ($xmlfile =~ /\*/) {
  print "Wildcards not supported: $xmlfile\n";
  exit 0;
}

die "Can't find input file \"$xmlfile\""
    unless -f $xmlfile;



$in_error=0;
$error_count=0;

$val_error="";

#print "Parsing $xmlfile\n";
my $parser = new XML::Parser(ErrorContext => 2, 
                             ProtocolEncoding => "UTF-8",
                             Handlers => {Start => \&start_handler,
                                          End => \&end_handler,
                                          Char => \&char_handler,
                                          Default => \&default_handler}
                             );


$parser->parsefile($xmlfile);

exit 0 if ( $error_count == 0 );

print "${val_error}\n";
exit 1;



sub start_handler
{
  my $p = shift;
  my $el = shift;

  $in_error = 0;

#print "DOC=$val_doc_id el=${el}\n";

  if ( $el eq "Error" ) {
  	$in_error = 1;
	$error_count = $error_count + 1;

    while (@_) {
      my $att = shift;
      my $attval = shift;
      if ($att eq "Description") {
        $val_error = "$val_error\n$attval";
      }
    }

  }
}


sub char_handler
{
	my ($p, $foo) = @_;

	my $data=$foo;

	# remove any tabs
#	$data =~ s/\t//g;

#	if ( $in_error ) {
#	}
}


sub end_handler
{
  my $p = shift;
  my $el = shift;

#print "CLOSE ${el}\n";

  if ( $el eq "Error" ) {
	  $in_error = 0;
  }
}


sub default_handler
{
  my $p = shift;
  my $data = shift;
}
