set nocount on
set transaction isolation level read uncommitted

select CertificationState, SUBSTRING(LegalForm, 1, 58) AS LegalForm, COUNT(*) as [Count]
FROM
	ipid_cici
GROUP BY CertificationState, LegalForm
ORDER BY CertificationState, LegalForm
