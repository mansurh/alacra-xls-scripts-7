#!c:/perl/bin/perl
use LWP::Simple;
use XML::Parser;

die "Usage: sitemapcheck.pl sitemapurl"
    unless (@ARGV == 1);

my $in_loc=0;
my $in_lastmod=0;
my $in_hint=0;
my $loc="";
my $lastmod="";
my $hint="";
my $mdfile=0;

my $sitemapurl=$ARGV[0];
my $sitemapfile="sitemap.xml";
my $errstr="";

my $savefile=$sitemapfile;
if ( $sitemapurl =~ /\.gz$/ ) {
  $savefile .= ".gz";
}

my %validhints;
$validhints{"always"}=1;
$validhints{"hourly"}=1;
$validhints{"daily"}=1;
$validhints{"weekly"}=1;
$validhints{"monthly"}=1;
$validhints{"yearly"}=1;
$validhints{"never"}=1;

system("rm -f $sitemapfile $savefile");

print ("$sitemapurl ");

$rc=getstore($sitemapurl, $savefile);

if ( $rc != 200 ) {
  print "$rc HTTP err on retrieve\n";
  exit (0);
}

if ( ! -s $savefile ) {
  print "Error downloading file (0 size)!\n";
  exit 1;
}

if ( $sitemapurl =~ /\.gz$/ ) {
  system("gzip -d $savefile");
}

my $parser = new XML::Parser(ErrorContext => 2,
                             ProtocolEncoding => "UTF-8",
                             Handlers => {Start => \&start_handler,
                                          End => \&end_handler,
                                          Char => \&char_handler}
                             );

$parser->parsefile($sitemapfile);

if ( $mdfile == 0 ) {
  $errstr .= "  metadata file not found!\n";
}

if ( $errstr eq "" ) {
  print "OK\n";
  exit 0;
} else {
  print "\n$errstr\n";
  exit 1;
}


sub start_handler
{
  my $p = shift;
  my $el = shift;

  if ($el eq "url") {
    $loc="";
    $lastmod="";
    $hint="";
  } elsif ($el eq "loc") {
    $in_loc = 1;
  } elsif ($el eq "lastmod") {
    $in_lastmod=1;
  } elsif ($el eq "changefreq") {
    $in_hint=1;
  }
}

sub end_handler
{
  my $p = shift;
  my $el = shift;

  if ($el eq "url") {
    # print "$loc $lastmod $hint\n";
    # do tests
    $item = $loc;
    $item =~ s/.+\/([^\/]+)$/$1/g;

    if ($loc eq "") {
      $errstr .= "  $item: empty loc in sitemapindex\n";
    } elsif ($loc !~ /http:\/\/[^\/]+\/storecontent\/.+/ ) {
      $errstr .= "  $item: item must have /storecontent/ in URL\n";
    } 

    if ($lastmod !~ /^[12][0-9][0-9][0-9]-[01][0-9]-[0-3][0-9]$/ ) {
      $errstr .= "  $item: lastmod must by YYYY-MM-DD format\n";
    } 
    
    if ($hint ne "") {
      $temp=$validhints{$hint};
      if ($temp != 1) {
        $errstr .= "  $item: changefreq not recognized value\n";
      }
    }

    if ($loc =~ /.+\.gpx(\.gz)?$/) {
      $mdfile=1;
    }
  } elsif ($el eq "loc") {
    $in_loc = 0;
  } elsif ($el eq "lastmod") {
    $in_lastmod=0;
  } elsif ($el eq "changefreq") {
    $in_hint = 0;
  }

}

sub char_handler
{
  my ($p, $data) = @_;

  if ($in_loc == 1) {
    $loc .= $data;
  } elsif ($in_lastmod == 1) {
    $lastmod .= $data;
  } elsif ($in_hint == 1) {
    $hint .= $data;
  }
}
