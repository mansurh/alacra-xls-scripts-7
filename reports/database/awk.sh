BEGIN{ counter = 1 ; }
{
	if( (NR % 2) == 0){ stats[counter++,4] = $1 ; }
	else{ 
		stats[counter,1] = $1 ;
		stats[counter,2] = $2 ;
		stats[counter,3] = $3 ;
	}
}
END{
	for( i = 1; i < counter ; i++ ){
		for( c = 1; c <= 4 ; c++ ){
			printf("%s ",stats[i,c]);
		}
		printf("\n");
	}
}
	
