set transaction isolation level read uncommitted
set nocount on

DECLARE @month datetime
select @month=CONVERT(datetime,(CONVERT(varchar(7),DATEADD(month,-1,GETDATE()),120)+'-01'))

select 
'Firm', 
'First Name', 
'Last Name',
DATENAME(month,DATEADD(month,-5,@month)),
DATENAME(month,DATEADD(month,-4,@month)),
DATENAME(month,DATEADD(month,-3,@month)),
DATENAME(month,DATEADD(month,-2,@month)),
DATENAME(month,DATEADD(month,-1,@month)),
DATENAME(month,@month),
'Last Download',
'Login'