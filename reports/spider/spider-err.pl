use Win32::OLE;
use OLE;

if (@ARGV != 4) {
  print "Usage: spider-err.pl server username passwd error-code\n";
  exit(1);
}

my $server=$ARGV[0];
my $user=$ARGV[1];
my $passwd=$ARGV[2];
my $errcode=$ARGV[3];

my $conn=CreateObject OLE "ADODB.Connection" || die "CreateObject: $!"; 
my $connect="DRIVER={SQL SERVER};PWD=${passwd};UID=${user};SERVER=${server};DATABASE=controldb"; 

$conn->open($connect);

my $sql="Select s.site_id, s.xls_id, s.name, s.url from control_errs c, sites s where c.site_id=s.site_id and c.error=${errcode} order by s.name ";
my $rs=$conn->Execute($sql) || die "Execute Error: $!";

print "\n";

if ($errcode == 7) {
  print "MISC. ERRORS\n";
} elsif ($errcode == 9) {
  print "NO HOST FOUND\n";
} elsif ($errcode == 10) {
  print "SERVER ERROR\n";
} elsif ($errcode == 12) {
  print "FILE NOT FOUND\n";
} elsif ($errcode == 13) {
  print "FILE FORBIDDEN\n";
} else {
  print "ERROR ${errcode}\n";
}

print "SITE_ID|XLS_ID|NAME|VALUE\n";
while ( !$rs->EOF){
	print $rs->Fields('site_id')->value;
	print "|";
	print $rs->Fields('xls_id')->value;
	print "|";
	print $rs->Fields('name')->value;
	print "|";
	print $rs->Fields('url')->value;
	print "\n";
	$rs->MoveNext;
}

$rs->Close;
$conn->Close;
