set nocount on
set transaction isolation level read uncommitted

select COALESCE([State], '(none)') as [State], COUNT(*) as [Count]
FROM
	ipid_gei
GROUP BY State
ORDER BY COUNT(*) desc
