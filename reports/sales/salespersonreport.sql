declare @startdate smalldatetime
declare @enddate smalldatetime


SET NOCOUNT ON

/* Run for previous month */
select @startdate = DATEADD(month, -1, GETDATE())

/* Move to beginning of month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)


PRINT ".XLS Sales Report for Commissioning Purposes"
PRINT ""
PRINT ""
select CONVERT(varchar(16),@startdate,107) "For Month Starting"
PRINT ""
PRINT ""

PRINT "Billable Downloads"
PRINT "==================================="
select 
	convert (varchar(40),COALESCE (p.company, a.name)) "Company/Account Name", ",",
	convert (varchar(16),p.last_name) "User Last Name", ",",
	convert (varchar(16),i.name) "Content Provider", ",",
	convert (varchar(16),s.last_name) "Salesperson Last Name", ",",
	convert (varchar(40),u.description) "Description", ",",
	convert (varchar(10),u.access_time, 1) "Date", ",",
	u.list_price "List Price", ",",
	u.price "Price", ","
from 
	usage u, users p, ip i, salesperson s, account a
where
	u.access_time >= @startdate
and
	u.access_time < @enddate
and
	u.userid = p.id
and
	u.ip = i.id
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	p.account = a.id
and	
	s.id = p.salesrep
and
	s.id in (39,40,42,47,48,62,64)
and	
	u.no_charge_flag is null

order by
	s.last_name, COALESCE (p.company, a.name)  
compute sum(u.price), 
	sum(u.list_price)
by
	s.last_name, COALESCE (p.company, a.name) 
compute sum(u.price), 
	sum(u.list_price)
by
	s.last_name 
go
