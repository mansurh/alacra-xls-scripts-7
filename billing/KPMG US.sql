select "KPMG US Usage"

select 
	i.ipcode, p.account, p.login, u.project,u.project2,u.project3,u.project4, u.project5,u.access_time,u.description,u.list_price
from 
	usage u, ip i, users p
where
	u.userid in (select id from users where account in (select id from account where name like "%kpmg%" and billingCenter="US") and demo_flag is NULL)
and
	u.ip = i.id
and
	u.userid = p.id
and
	access_time >= "January 1, 2004" 
and 
	access_time < "February 1, 2004"
and
	u.price=0.0
and
	u.ip in (68,83,124,161,189,241)
and
	remote_address <> "127.000.000.001"
order by access_time desc