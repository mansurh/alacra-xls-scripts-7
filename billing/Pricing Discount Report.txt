print 'usage discount report'
select d.access_time 'date', o.orderid 'order', p.login, a.name 'account', u.project2 'affiliate', d.description 'discount code', pd.discountName 'discount description', CASE WHEN pd.royalty_split=1 THEN 'Y' ELSE 'N' END 'royalty split', u.ip, u.description 'download description', u.price 'pre-discount price', d.price 'discount' 
from
usage d, usage u, users p, account a, pricing_discounts pd, cc_orderitem o
where
d.access_time > 'November 1, 2008' and d.price < 0
and
d.shoppingcartid = u.shoppingcartid
and
d.usageid <> u.usageid
and
u.userid = p.id and p.account = a.id
and
d.description = pd.discountToken
and
d.shoppingcartid = o.shoppingcartid
order by d.access_time asc
