scriptdir=$XLS/src/scripts/sitemaps
for file in *.smx.gz
do
  if [ $file != "*.smx.gz" ]
  then
    echo ${file}
    gzip -d ${file}
    file=${file%.gz}
    perl ${scriptdir}/fixMetadataRef.pl <${file} >${file}.tmp
    mv ${file}.tmp ${file}
    gzip ${file}
  fi
done

for file in *.smx
do
  echo ${file}
  perl ${scriptdir}/fixMetadataRef.pl <${file} >${file}.tmp
  mv ${file}.tmp ${file}
done