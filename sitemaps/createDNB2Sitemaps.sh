XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

if [ $# -lt 3 ]
then
	echo "usage: $0 server {post sitemaps; 1 for yes} {load_invIndexSortFull_table; 1 for yes} [opt: default YYYY-MM-DD date] [opt: max url entries]"
	exit 1
fi

dnbserver=$1

if [ "$2" = "1" ]
then
	POST_SITEMAPS=1
fi

if [ "$3" = "1" ]
then
	LOAD_INV_INDEX_SORT_TABLE=1
fi	
	
DEF_DATE=$4
MAX_URLS=$5

database=dnb2
dnbuser=`get_xls_registry_value ${database} User`
dnbpassword=`get_xls_registry_value ${database} Password`

database=sitemapdb
siteserver=`get_xls_registry_value ${database} Server`
siteuser=`get_xls_registry_value ${database} User`
sitepassword=`get_xls_registry_value ${database} Password`

database=xls
xlsserver=`get_xls_registry_value ${database} Server`
xlsuser=`get_xls_registry_value ${database} User`
xlspassword=`get_xls_registry_value ${database} Password`

echo "begin"

run() {
#this first part is light on db so no need to do startupdate yet
	echo "Getting existing duns numbers from company table - these are tier 1"
	
	bcp "select id, duns from company where len(duns)=9" queryout xlsdunsno.pip /S$xlsserver /U$xlsuser /P$xlspassword /c /t"|"
	check_return_code $? "Error bcp out of production duns numbers from $xlsserver, exiting ..." $?

	isql /U${dnbuser} /P${dnbpassword} /S${dnbserver} /b /Q "drop table xlsdunsno"
	check_return_code $? "Could not drop xlsdunsno table, exiting ..." $?
	
	echo creating xlsdunsno table
	isql  /U${dnbuser} /P${dnbpassword} /S${dnbserver} /b /Q "create table xlsdunsno ( xlsid integer not null, dunsno integer not null ) create index xlsdunsno01 on xlsdunsno ( dunsno )"
	check_return_code $? "Error creating xlsdunsno table, exiting ..." $?

	bcp xlsdunsno in xlsdunsno.pip  /U${dnbuser} /P${dnbpassword} /S${dnbserver} /exlsdunsno.err /c /t"|"
	check_return_code $? "Error bcp in of xlsdunsno, exiting ..." $?

	echo "Count of companies in sort table:"
	isql  /U${dnbuser} /P${dnbpassword} /S${dnbserver} /Q "select count(*) from invIndexSortFull" 


#  $XLS/src/scripts/loading/dba/startupdate.sh dnb2 ${COMPUTERNAME} sitemaps
#  check_return_code $? "error in startupdate.sh, exiting ..." $?

  if [ "$LOAD_INV_INDEX_SORT_TABLE" = "1" ]
  then
  	echo "Creating and populating inverted index sort table for all dnb companies"
  
	isql  /U${dnbuser} /P${dnbpassword} /S${dnbserver} /b -n << HERE
set rowcount 0
drop table invIndexSortFull
SELECT 
IDENTITY(int, 1,1) sort, dunsNo, 0 indexed, 
case 
when (w.dunsno in (select x.dunsno from xlsdunsno x where x.dunsno=w.dunsno) ) then 1
when (w.salesAnnUSD >= 1000000 or (w.employeesTotal >= 200 and w.countryCode IN (121, 805, 785, 793, 797, 801)) or w.dunsno in (select x.dunsno from xlsdunsno x where x.dunsno=w.dunsno) ) then 2 
when (w.salesAnnUSD >= 500000 or w.employeesTotal >= 50 ) then 3 
when  (salesAnnUSD >= 100000 or employeesTotal >= 10 ) then 4
else 5 end tier
INTO invIndexSortFull
from worldbase w, type t, legalStatus l, dnbToXlsCountryMap cm
where w.TypeOfEstablishment = t.Type and w.legalStatusCode *= l.code and w.CountryCode=cm.dnbCountryCode
order by tier, w.name desc , w.tradestyle desc , dunsno desc
HERE
	check_return_code $? "error creating invIndexSortFull table, exiting ..." $?

  fi

  # where is the index located?
  currentloc=`reginvidx get dnb2store`

  scratch=${currentloc%/*}
  indexdir=${scratch}

  cd $indexdir
  mkdir -p new
  cd new
  check_return_code $? "error in cd to ${indexdir}/new, exiting ..." $?

  rm -f *smx*

  echo "creating sitemaps"
  #####c:/programfiles/perl/bin/perl.exe $XLS/src/scripts/sitemaps/createDnb2Sitemaps.pl ${dnbserver} dnb2 ${dnbuser} ${dnbpassword} $COMPUTERNAME $DEF_DATE $MAX_URLS
  perl $XLS/src/scripts/sitemaps/createDnb2Sitemaps.pl ${dnbserver} dnb2 ${dnbuser} ${dnbpassword} $COMPUTERNAME $DEF_DATE $MAX_URLS
  check_return_code $? "error in perl createDnb2Sitemaps.pl, exiting ..." $?

	#sanity check (make sure you have at least 100 files)
	if [ `ls *smx* |wc -l` -lt 100 ]
	then
		echo "createDnb2Sitemaps.pl created less than 100 sitemaps, exiting ..."
		exit 1
	fi

	test -s sitemap.sql
	check_return_code $? "sitemap.sql is empty, exiting ..." $?

	if [ "$POST_SITEMAPS" = "1" ]
	then
		echo "posting sitemaps"
		isql /S${siteserver} /U${siteuser} /P${sitepassword} /b < sitemap.sql
		check_return_code $? "error running sitemap.sql, exiting ..." $?
		rm -f ${indexdir}/*smx*
		mv ${indexdir}/new/*smx* ${indexdir}/
	fi
	
#  	$XLS/src/scripts/loading/dba/endupdate.sh dnb2 ${COMPUTERNAME} sitemaps
  
}

mkdir -p $XLSDATA/index
logfilename=$XLSDATA/index/sitemapindex`date +%y%m%d`.log

`run > ${logfilename} 2>&1`
returncode=$?
if [ $returncode -ne 0 ]
then
	blat ${logfilename} -t administrators@alacra.com -s "dnb2 direct sitemap index generation failed"
	exit ${returncode}
fi
