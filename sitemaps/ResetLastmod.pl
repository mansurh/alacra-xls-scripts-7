#!c:/perl/bin/perl
use XML::Parser;

die "Usage: resetlastmod.pl sitemapfile YYYY-MM-DD outputfile"
    unless (@ARGV == 3);

my $in_loc=0;
my $in_lastmod=0;
my $in_hint=0;
my $loc="";
my $loc_id="";
my $lastmod="";
my $hint="";
my $mdfile=0;

my $sitemapfile=$ARGV[0];
my $newdate=$ARGV[1];
my $outfile=$ARGV[2];
my $errstr="";

@fileparts = split(/_/, $sitemapfile);
my $dbname=$fileparts[0];

if ( ! -s $sitemapfile ) {
  print "Error finding file\n";
  exit 1;
}

my $OUTFILE;

die "Unable to open file for output"
    unless open(OUTFILE, ">$outfile");

binmode OUTFILE;
my $changeCount=0;
my $parser = new XML::Parser(ErrorContext => 2,
                             ProtocolEncoding => "UTF-8",
                             Handlers => {Start => \&start_handler,
                                          End => \&end_handler,
                                          Char => \&char_handler}
                             );

print OUTFILE "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
$parser->parsefile($sitemapfile);
print "$changeCount dates changed\n";
close OUTFILE;

sub start_handler
{
  my $p = shift;
  my $el = shift;

  print OUTFILE "<${el}";
  if ($el eq "url") {
    $loc="";
    $loc_id="";
    $lastmod="";
    $hint="";
  } elsif ($el eq "loc") {
    $in_loc = 1;
  } elsif ($el eq "lastmod") {
    $in_lastmod=1;
  } elsif ($el eq "changefreq") {
    $in_hint=1;
  }

  $attrName=shift;
  while ($attrName ne "") {
    $attrVal=shift;
    print OUTFILE " $attrName=\"$attrVal\"";
    $attrName = shift;
  }

  print OUTFILE ">";
}

sub end_handler
{
  my $p = shift;
  my $el = shift;

  if ($el eq "loc") {
    $loc_id = $loc;
    $loc_id =~ s!http://www.alacrastore.com/storecontent/!!g;
    $in_loc = 0;
    print OUTFILE $loc;
  } elsif ($el eq "lastmod") {
    $in_lastmod=0;
    print OUTFILE $newdate;
    $changeCount++;
  } elsif ($el eq "changefreq") {
    print OUTFILE $hint;
    $in_hint = 0;
  }

  print OUTFILE "</$el>";
  if ($el eq "url") {
    print OUTFILE "\n";
  }

}

sub char_handler
{
  my ($p, $data) = @_;

  if ($in_loc == 1) {
    $loc .= $data;
  } elsif ($in_lastmod == 1) {
    $lastmod .= $data;
  } elsif ($in_hint == 1) {
    $hint .= $data;
  }
}
