# copydatabase.sh
# Parse the command line arguments.
#	1 = source db name
#	2 = source server
#	3 = source user
#	4 = source psw
#	5 = dest db name
#	6 = dest server
#	7 = dest user
#	8 = dest psw
#	9 = schema path
#	10 = table exclusion list file (optional)
#	11 = procedure exclusion list file (optional)
#	12 = incremental dump list file (optional) -- used to dump selected rows out of large tables

shname=$0
if [ $# -lt 9 ]
then
	echo "Usage: ${shname} sourcedb sourceserver sourceuser sourcepsw destdb destserver destuser destpsw ourpath [tableexclfile] [procexclfile]"
	exit 1
fi

# Save the runstring parameters in local variables
sourcedb=${1}
sourceserver=${2}
sourceuser=${3}
sourcepsw=${4}
destdb=${5}
destserver=${6}
destuser=${7}
destpsw=${8}
ourpath=${9}

if [ $# -gt 9 ]
then
	tableexclfile=${10}
else
	tableexclfile="none"
fi

if [ $# -gt 10 ]
then
	procexclfile=${11}
else
	procexclfile="none"
fi

if [ $# -gt 11 ]
then
	incrdumpfilelist=${12}
else
	incrdumpfilelist="none"
fi

# sign on
echo "${shname} settings:"
echo "    sourcedb=$sourcedb"
echo "    sourceserver=$sourceserver"
echo "    sourceuser=$sourceuser"
#echo "    sourcepsw=$sourcepsw"
echo "    destdb=$destdb"
echo "    destserver=$destserver"
echo "    destuser=$destuser"
#echo "    destpsw=$destpsw"
echo "    ourpath=$ourpath"
echo "    tableexclfile=$tableexclfile"
echo "    procexclfile=$procexclfile"
echo ""

changelist=changelist.txt
repltables=mergearticles.txt

# change to the work directory
cd ${ourpath}
if [ $? -ne 0 ]
then
	echo "${shname}: Could not change to ${ourpath}"
	exit 1
fi

# remove any existing ${sourcedb} subdirectory
if [ -d ${sourcedb} ]
then
	echo "${shname}: Removing existing ${sourcedb} subdirectory from ${ourpath}"
	rm -fR ${sourcedb}
fi
if [ -d ${sourcedb} ]
then
	echo "${shname}: Could not remove existing ${sourcedb} subdirectory from ${ourpath}"
	exit 1
fi

# run the sqldump utility
echo "${shname}: Running generate_schema.ps1"
powershell $XLS/schema/generate_schema.ps1 ${sourceserver} ${sourcedb} ${sourceuser} ${sourcepsw} 
rc=$?
if [ $rc != 0 ]
then
	echo "${shname}: Error $rc returned from from generate_schema.ps1"
	exit 1
fi
echo ""


# OK, now we gots to get rid of all the exclusions
if [ "${tableexclfile}" = "none" ]
then
	echo "${shname}: No table exclusion file specified"
fi

#remove stored procedures used for replication
rm -r -f ${sourcedb}/procedures/MSmerge*

if [ ${procexclfile} = "none" ]
then
	echo "${shname}: No procedure exclusion file specified"
fi
echo ""

#retrieve replicated tables, add them to exclude list, but also back them up to disk
bcp "select name from sysmergearticles order by name" queryout ${repltables} -S${sourceserver} -U${sourceuser} -P${sourcepsw} /c
if [ $? -ne 0 ]; then echo "Error retrieving sysmergearticles tables, exiting ..."; exit 1; fi

origexclfile=${tableexclfile}.orig
touch ${origexclfile}
if [ "${tableexclfile}" = "none" ]
then
	if [ -s ${repltables} ]; then tableexclfile=${repltables}; fi;  
else
	cp $tableexclfile ${origexclfile}
	cat ${repltables}  >> $tableexclfile
fi

bcp "select o.name, sum(d.row_count) from sys.dm_db_partition_stats d, sysobjects o where d.object_id=o.id and d.index_id < 2 and o.xtype='U' and o.name not like 'MSMerge%' group by o.name order by o.name" queryout ${sourceserver}.txt /U${sourceuser} /P${sourcepsw} /S ${sourceserver} /c
if [ $? != 0 ]; then echo "error extracting table list from $sourceserver"; exit 1; fi;

bcp "select o.name, sum(d.row_count) from sys.dm_db_partition_stats d, sysobjects o where d.object_id=o.id and d.index_id < 2 and o.xtype='U' and o.name not like 'MSMerge%' group by o.name order by o.name" queryout ${destserver}.txt /U${destuser} /P${destpsw} /S ${destserver} /c
if [ $? != 0 ]; then echo "error extracting table list from $destserver"; exit 1; fi;

diff --changed-group-format='%<' --unchanged-group-format='' ${sourceserver}.txt ${destserver}.txt  |cut -f1 > ${changelist}
if [ $? != 0 ]; then echo "error getting change list from $destserver"; exit 1; fi;

# DROP all key constraints on tables
echo "${shname}: DROPPING KEY CONSTRAINTS"
for filename in `ls ${sourcedb}/tables`
do
	# if this is a directory
	if [ -d ${sourcedb}/tables/${filename} ]
	then
		if [ "${tableexclfile}" != "none" ]; then 
			grep -i -q -w $filename ${tableexclfile}
			if [ $? -eq 0 ]; then continue; fi
		fi
		dropconstraintdir=${sourcedb}/tables/${filename}/DROPconstraints
		if [ -d ${dropconstraintdir} ]
		then
			for constraint in `ls ${dropconstraintdir}`
			do
				conname=${constraint%.sql}

				echo "${shname}: Dropping constraint ${conname} from table ${filename}"
				isql -b -S ${destserver} -U ${destuser} -P ${destpsw} -d ${destdb} < ${dropconstraintdir}/${constraint}
				if [ $? != 0 ]
				then
					echo "${shname}: Error dropping constraint ${conname}, SQL file ${constraint}"
					exit 1
				fi
			done
		fi
	fi
done


# Now loop over all tables: drop, recreate, and transfer data
echo "${shname}: DROPPING AND RECREATING TABLES"
#for filename in `ls ${sourcedb}/tables`
while read filename
do
	# if this is a directory
	if [ -d ${sourcedb}/tables/${filename} ]
	then
		if [ "${tableexclfile}" != "none" ]; then 
			grep -i -q -w $filename ${tableexclfile}
			if [ $? -eq 0 ]; then
				echo "Excluding ${filename} from copy ..."
				continue; 
			fi
		fi
		echo "${shname}: Dropping and recreating table ${filename}"
		isql -b -S ${destserver} -U ${destuser} -P ${destpsw} -d ${destdb} < ${sourcedb}/tables/${filename}/create.sql
		if [ $? != 0 ]
		then
			echo "${shname}: Error dropping and recreating table ${filename}"
			exit 1
		fi

		# now try and copy the data
		echo "Current Time for ${filename} copy:" `date`
		echo "${shname}: Transferring data for table ${filename}"
		tempfile=${ourpath}/${sourcedb}/tables/${filename}/data.bcp
		$XLS/src/scripts/transfer/transferdataonly.sh ${filename} ${sourcedb} ${sourceserver} ${sourceuser} ${sourcepsw} ${filename} ${destdb} ${destserver} ${destuser} ${destpsw} ${tempfile}
		if [ $? != 0 ]
		then
			echo "${shname}: Error transferring data for table ${filename}"
			exit 1
		fi
		echo ""
#		rm -f ${tempfile}

	fi
done < ${changelist}
#done #for filename

typeset -i errorcount
errorcount=0
echo "${shname}: RECREATING INDICES"
#for filename in `ls ${sourcedb}/tables`
while read filename
do
	# if this is a directory
	if [ -d ${sourcedb}/tables/${filename} ]
	then
		if [ "${tableexclfile}" != "none" ]; then 
			grep -i -q -w $filename ${tableexclfile}
			if [ $? -eq 0 ]; then continue; fi
		fi
		# lastly, restore the indexes & triggers
		if [ -e ${sourcedb}/tables/${filename}/create_index.sql ]
		then
			echo "Current Time for ${filename} indexing:" `date`
			echo "${shname}: Recreating indexes and triggers for table ${filename}"
			isql -b -S ${destserver} -U ${destuser} -P ${destpsw} -d ${destdb} < ${sourcedb}/tables/${filename}/create_index.sql
			if [ $? != 0 ]
			then
				errorcount=`expr $errorcount + 1`
				echo "${shname}: Error recreating indexes and triggers for table ${filename}: error count ${errorcount}"
				if [ $errorcount -gt 5 ]
				then
					exit 1
				fi
			fi
		fi
		triggerdir=${sourcedb}/tables/${filename}/triggers
		if [ -d ${triggerdir} ]
		then
			for trigger in `ls ${triggerdir}`
			do
				trname=${trigger%.sql}

				echo "${shname}: Restoring trigger ${trname} on table ${filename}"
				echo "  isql -S ${destserver} -U ${destuser} -d ${destdb} < ${triggerdir}/${trigger}"
				isql -b -S ${destserver} -U ${destuser} -P ${destpsw} -d ${destdb} < ${triggerdir}/${trigger}
				if [ $? != 0 ]
				then
					echo "${shname}: Error restoring trigger ${trname}, SQL file ${trigger}"
					exit 1
				fi
			done
		fi
	fi
done < ${changelist}
#done #for filename

echo "Time after indexing:" `date`
# RESTORE all key constraints on tables
echo "${shname}: RECREATING KEY CONSTRAINTS"
for filename in `ls ${sourcedb}/tables`
do
	# if this is a directory
	if [ -d ${sourcedb}/tables/${filename} ]
	then
		if [ "${tableexclfile}" != "none" ]; then 
			grep -i -q -w $filename ${tableexclfile}
			if [ $? -eq 0 ]; then continue; fi
		fi
		constraintdir=${sourcedb}/tables/${filename}/constraints
		if [ -d ${constraintdir} ]
		then
			for constraint in `ls ${constraintdir}`
			do
				conname=${constraint%.sql}

				echo "${shname}: Restoring constraint ${conname} on table ${filename}"
				echo "  isql -S ${destserver} -U ${destuser} -d ${destdb} < ${constraintdir}/${constraint}"
				isql -b -S ${destserver} -U ${destuser} -P ${destpsw} -d ${destdb} < ${constraintdir}/${constraint}
				if [ $? != 0 ]
				then
					echo "${shname}: Error restoring constraint ${conname}, SQL file ${constraint}"
					exit 1
				fi
			done
		fi
	fi
done


echo "Time after constraints:" `date`

# Now recreate stored procedures, if there are any
echo "${shname}: DROPPING AND RECREATING STORED PROCEDURES"
for filename in `ls ${sourcedb}/procedures/*.sql`
do
	# if this is a regular file
	if [ ! -d ${filename} ]
	then
		xprocname=${filename##*/}
		procname=${xprocname%.sql}
		if [ "${procexclfile}" != "none" ]; then 
			grep -i -q -w $procname ${procexclfile} 
			if [ $? -eq 0 ]; then
				echo "Excluding procedure $procname from copy ..."
				continue; 
			fi
		fi

		echo "${shname}: Dropping and recreating procedure ${procname}"
#		echo "  isql -S ${destserver} -U ${destuser} -d ${destdb} < ${filename}"
		isql -b -S ${destserver} -U ${destuser} -P ${destpsw} -d ${destdb} < ${filename}
		if [ $? != 0 ]
		then
			echo "${shname}: Error dropping and recreating procedure ${procname}"
			exit 1
		fi
	fi
done

echo "Time after procedures:" `date`

# Now loop over all replicated tables, and dump data to disk
echo "${shname}: DUMPING REPLICATED DATA TABLES"
while read filename 
do
	grep -i -q -w $filename ${origexclfile}
	if [ $? -eq 0 ]; then echo "skipping $filename from dumping"; continue; fi

	echo "Current Time for ${filename} copy:" `date`
	echo "${shname}: Dumping data for table ${filename}"
	tempfile=${ourpath}/${sourcedb}/tables/${filename}/data.bcp
	$XLS/src/scripts/transfer/dumpdataonly.sh ${filename} ${sourcedb} ${sourceserver} ${sourceuser} ${sourcepsw} ${tempfile}
	if [ $? != 0 ];	then echo "${shname}: Error dumping data for table ${filename}"; exit 1; fi
done < ${repltables}

echo "Time after replicated data:" `date`

if [ ${incrdumpfilelist} != "none" ]; then
	while read line
	do
		IFS='|' read -ra varr <<< "$line"
		filename=${varr[0]}
		query="select * from ${filename} where ${varr[1]}"
		tempfile=${ourpath}/${sourcedb}/tables/${filename}/data.bcp
		$XLS/src/scripts/transfer/dumpselectedrows.sh "${query}" ${sourcedb} ${sourceserver} ${sourceuser} ${sourcepsw} ${tempfile}
		if [ $? != 0 ];	then echo "${shname}: Error dumping incremental data for table ${filename}"; exit 1; fi
	done < ${incrdumpfilelist} 
	echo "Time after incremental data:" `date`
fi

echo ""
echo "DATABASE COPY COMPLETE"
if [ $errorcount -gt 0 ]
then
	echo "WARNING!!!!"
	echo "WARNING!!!! COPY ENCOUNTERED ${errorcount} ERRORS, DURING INDEXING OF TABLES "
	echo "WARNING!!!!"
fi

exit 0
