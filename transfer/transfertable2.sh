XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_bcp_errors.fn

#parse the command line arguments
# 1 = srctable
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest table
# 7 = dest db
# 8 = dest server
# 9 = dest user
# 10 = dest passwd

if [ $# -ne 10 ]
then
    echo "Usage: transferTable.sh srctable srcdb srcserver srclogin srcpasswd desttable destdb destserver destlogin destpasswd"
    exit 1
fi

srctable=$1
srcdb=$2
srcserver=$3
srcuser=$4
srcpasswd=$5
desttable=$6
destdb=$7
destserver=$8
destuser=$9
destpasswd=${10}


TEMPDIR=$XLSDATA
rm -f ${TEMPDIR}/${desttable}.dat

echo "Transfer of table from ${srcserver}.${srcdb}.${srctable}.${srcuser} to ${desttable} table in ${destserver}.${destdb}.${desttable}.${destuser}"

time bcp ${srcdb}.dbo.${srctable} out ${TEMPDIR}/${desttable}.dat /S${srcserver} /U${srcuser} /P${srcpasswd} /n /CRAW

if [ $? != 0 ]
then
    echo "Error saving source ${srctable} table data to file"
    exit 1
fi

time isql /b /U${destuser} /P${destpasswd} /S${destserver} /Q"use ${destdb} truncate table ${desttable}; ALTER INDEX ALL ON ${desttable} DISABLE"

if [ $? != 0 ]
then
    echo "Error truncating  ${desttable} in ${destdb}"
    exit 1
fi

echo "Loading data into destination ${desttable} table in ${destdb}..."

time bcp ${destdb}.dbo.${desttable} in ${TEMPDIR}/${desttable}.dat /S${destserver} /U${destuser} /P${destpasswd} /E /q /b1000 /o ${desttable}.errout /n /CRAW
if [ $? != 0 ]
then
    cat ${desttable}.errout
    echo "Error copying data to destination ${desttable} table in ${destdb}"
    exit 1
fi

cat ${desttable}.errout
check_bcp_errors ${desttable}.errout 1 "Failed to bcp ${desttable} table. Aborting" 1

time isql /b /U${destuser} /P${destpasswd} /S${destserver} /Q"ALTER INDEX ALL ON ${desttable} REBUILD"
if [ $? != 0 ]
then
    echo "Error rebuilding indexes on ${desttable} in ${destdb}"
    exit 1
fi


rm -f ${TEMPDIR}/${desttable}.dat
rm -f ${desttable}.errout