BEGIN { foundem=0 }
{
    if ($4 != "") {
		if ($4 != K) {
			x=$4
		} else {
			x=NEWKEY
			foundem++
		}

		if (NR == 1)
			printf "%s", x
		else
			printf ", %s", x
	}
}
END { print (foundem) >FLAGFILE }
