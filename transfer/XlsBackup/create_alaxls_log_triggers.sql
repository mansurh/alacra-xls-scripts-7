CREATE TRIGGER prepbooktoc_del
ON prepbooktoc        
FOR DELETE      
as
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, deleted, host, updatedate)      
  select prepbookid, 'prepbooktoc', '1', host_name(), getdate() from deleted       
END
go      
CREATE TRIGGER prepbooktoc_insupd      
ON prepbooktoc      
FOR UPDATE, INSERT      
AS 
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, host, updatedate)      
  select prepbookid, 'prepbooktoc', host_name(), getdate() from inserted       
END
go
CREATE TRIGGER prepbook_del      
ON prepbook        
FOR DELETE      
as
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, deleted, host, updatedate)      
  select prepbookid, 'prepbook', '1', host_name(), getdate() from deleted       
END
go      
CREATE TRIGGER prepbook_insupd      
ON prepbook      
FOR UPDATE, INSERT      
AS
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, host, updatedate)      
  select prepbookid, 'prepbook', host_name(), getdate() from inserted       
END
go
CREATE TRIGGER pbusage_insupd      
ON pbusage      
FOR UPDATE, INSERT      
AS
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, host, updatedate)      
  select pbusage, 'pbusage', host_name(), getdate() from inserted       
END
go
CREATE TRIGGER pbusage_nodelete          
ON pbusage            
FOR DELETE             
AS
IF (user_id() != 1) 
BEGIN
  RAISERROR ('Deletes not allowed from pbusage table', 16, 10)            
  ROLLBACK TRANSACTION        
END
go
CREATE TRIGGER usage_insupd    
ON usage    
FOR UPDATE, INSERT    
AS     
insert alaxls_log (id, table_name, host, updatedate)    
select usageid, 'usage', host_name(), getdate() from inserted     
go 
CREATE TRIGGER usage_nodelete    
ON usage      
FOR DELETE       
AS 
IF (user_id() != 1) 
BEGIN
  RAISERROR ('Deletes not allowed from usage table', 16, 10)      
  ROLLBACK TRANSACTION    
END
go
CREATE TRIGGER shoppingcart_del    
ON shoppingcart      
FOR DELETE    
as
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, deleted, host, updatedate)    
  select shoppingcartid, 'shoppingcart', '1', host_name(), getdate() from deleted     
  insert shoppingcart_delete (shoppingcartid,sk,app,msg,topic,productid,description,listprice,actualprice,applicableprice,userid,prepbookid,download_properties,scorder,dateentered,lwid,displayprice,vendorprice,ip,xmlversion,termsurl,failed,expires,expire_ttl,price_token,reason,encrypted,landingpageurl,appdocid)
    select shoppingcartid,sk,app,msg,topic,productid,description,listprice,actualprice,applicableprice,userid,prepbookid,download_properties,scorder,dateentered,lwid,displayprice,vendorprice,ip,xmlversion,termsurl,failed,expires,expire_ttl,price_token,reason,encrypted,landingpageurl,appdocid from deleted
END
go
CREATE TRIGGER shoppingcart_insupd    
ON shoppingcart    
FOR UPDATE, INSERT    
AS     
IF (user_id() != 1) 
BEGIN
  insert alaxls_log (id, table_name, host, updatedate)    
  select shoppingcartid, 'shoppingcart', host_name(), getdate() from inserted     
END
go
CREATE TRIGGER shoppingcartlog_nodelupd    
ON shoppingcartlog      
FOR DELETE, UPDATE
AS 
IF (user_id() != 1) 
BEGIN
  RAISERROR ('Deletes/updates not allowed from shoppingcartlog table', 16, 10)        
  ROLLBACK TRANSACTION      
END