# Script file to build the IP matching table for factset
# 1 = IPID Server
# 2 = IPID Database
# 2 = IPID Login
# 3 = IPID Password
# 4 = factset Server
# 5 = factset Login
# 6 = factset Password
ARGS=7 #Number of arguments expected
if [ $# -ne $ARGS ]
then
    echo "Usage: ipid_factset.sh destdataserver destdb destlogon destpassword srcserver srclogin srcpassword"
    exit 1
fi

destdataserver=$1
destdb=$2
destlogon=$3
destpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7

SCRIPTDIR=${XLS}/src/scripts/loading/lionshares

rm -f *.dat *.txt *.tmp

# retrieve new data
${SCRIPTDIR}/bofa_pvt.sh ${srcdataserver} ${srclogon} ${srcpassword} 
if [ $? != 0 ]
then

    echo "Error retrieving new data, exiting"
    exit 1

fi

# back up old data
isql -S ${destdataserver} -d ${destdb} -U ${destlogon} -P ${destpassword} -Q " drop table ipid_factset_prev "
isql -S ${destdataserver} -d ${destdb} -U ${destlogon} -P ${destpassword} -Q " exec sp_rename ipid_factset, ipid_factset_prev "
isql -S ${destdataserver} -d ${destdb} -U ${destlogon} -P ${destpassword} -Q " select top 0 * into ipid_factset from ipid_factset_prev "

# dump data to an bulk-data file
echo "dumping to output bulk data file..."
echo "bcp lionshares.dbo.bofa_pvt out ipid_factset.dat /U${srclogon} /P${srcpassword} /S${srcdataserver} /eipid_factset.err /m1000 /c /t\"|\""
bcp lionshares.dbo.bofa_pvt out ipid_factset.dat /U${srclogon} /P${srcpassword} /S${srcdataserver} /eipid_factset.err /m1000 /c /t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out ipid_factset.dat, exiting ..."
	exit 1
fi


echo "...done"

# now load that data to the concordance database (to the ipid table)
echo "loading bulk data file..."
echo "bcp ${destdb}.dbo.ipid_factset in ipid_factset.dat /U${destlogon} /P${destpassword} /S${destdataserver} /eipid_factset.err /m1000 /c /t\"|\""
bcp ${destdb}.dbo.ipid_factset in ipid_factset.dat /U${destlogon} /P${destpassword} /S${destdataserver} /eipid_factset.err /m1000 /c /t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp in ipid_factset.dat, exiting ..."
	exit 1
fi

isql -S ${destdataserver} -d ${destdb} -U ${destlogon} -P ${destpassword} -Q " create index ipid_factset01 on ipid_factset(FDS_ID)"

echo "...done"

echo ipid_factset.dat

exit 0
