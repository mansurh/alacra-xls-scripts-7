# Script file to build the IP matching table for snl
# 1 = IPID Server
# 2 = IPID Server
# 3 = IPID Login
# 4 = IPID Password
# 5 = snl Server
# 6 = snl Login
# 7 = snl Password
ARGS=7
if [ $# -lt $ARGS ]
then
    echo "Usage: ipid_snl.sh ipid_server ipid_database ipid_login ipid_password snl_server snl_login snl_password"
    exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogon=$3
IPIDpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7


LOADDIR=${XLS}/src/scripts/loading/snl
TEMPDIR=${XLSDATA}/matching


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${srcdataserver}"
echo ""

isql -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -Q "set nocount on select distinct d1.doc_id + '|' + substring(d1.prop_value,charindex(':',d1.prop_value)+1,64) + '|' + d2.prop_value from snl.dbo.doc_props d1, snl.dbo.doc_props d2 where d1.prop_name='name' and d2.prop_name='ticker'  and d1.doc_id=d2.doc_id " -w 255 -x 255 > ${TEMPDIR}/ipid_snl.dat.temp1

tail +9 < ${TEMPDIR}/ipid_snl.dat.temp1 | sed -f ${LOADDIR}/ipid_snl.sed > ${TEMPDIR}/ipid_snl.dat.temp2

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        print ${line}

    fi

done < ${TEMPDIR}/ipid_snl.dat.temp2 > ${TEMPDIR}/ipid_snl.dat

#
# Drop old data from xls table ipid_snl
#

echo ""
echo "Dropping old ipid_snl data on ${IPIDserver}"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/create_ipid_snl.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_snl for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/dropindex_ipid_snl.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_snl.dat"

# Load the ipid_snl table
echo "Loading the ipid_snl table" 

bcp concordance.dbo.ipid_snl in ${DATFILE} /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} /f${LOADDIR}/ipid_snl.fmt /e${TEMPDIR}/ipid_snl.log -m 2000
if [ $? != 0 ]
then

    echo "Error loading ipid_snl table"
    exit 1

fi

# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_snl for faster loading"
echo ""

isql /U${IPIDlogon} /P${IPIDpassword} /S${IPIDserver} < ${LOADDIR}/createindex_ipid_snl.sql

echo ""
echo "Updated table ipid_snl for snl OK."
echo ""

exit 0
