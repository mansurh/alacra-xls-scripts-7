# Script file to build the IP matching table for wscope
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = wscope Server
# 6 = wscope Login
# 7 = wscope Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_wscope.sh ipid_server ipid_database ipid_login ipid_password wscope_server wscope_login wscope_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
wscopeserver=$5
wscopelogin=$6
wscopepassword=$7

SCRIPTDIR=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/wscope
mkdir -p ${DATADIR}
cd ${DATADIR}

# Name of the temp file to use
TMPFILE1=ipid_wscope1.tmp
TMPFILE2=ipid_wscope2.tmp

# Name of the table to use
TABLENAME=ipid_wscope

# Name of the format file to use
FORMAT_FILE=${SCRIPTDIR}/ipid_wscope.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2} ${TABLENAME}.err

# Step 2 - select the wscope data into a temporary file
echo downloading company info from $wscopeserver
isql -S${wscopeserver} -U${wscopelogin} -P${wscopepassword} -s"|" -n -w500 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON          
select c.id, c.name, c.ticker, c.cusip, c.shareType, c.sedol, c.ISIN, 'city', (select co.name from country co where co.id=c.nation), 
c.entityType, c.dateAdded 
from  company c where inactiveDate is null
HERE

# Step 3 - post-process the temp file           
echo processing file in pip
sed -e "s/  *|/|/g;s/  *$//g;s/NULL//g" ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist      
echo dropping $TABLENAME
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
echo creating $TABLENAME
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	id varchar(16),
	name varchar(120),
	ticker varchar(10) null,
	cusip varchar(10) null,
	shareType varchar(120) null,
	sedol char(10) null,
	ISIN char(16) null,
	city varchar(32) null,
	country varchar(32)null,
	entityType char(1) null,
	dataAdded char(10) null
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(id)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(country)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /f${FORMAT_FILE} /b1000 /m1000 /e${TABLENAME}.err
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
