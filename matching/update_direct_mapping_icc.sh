XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

xlsserver=`get_xls_registry_value concordance Server`
xlslogin=`get_xls_registry_value concordance User`
xlspassword=`get_xls_registry_value concordance Password`
ipserver=`get_xls_registry_value icc Server`
iplogin=`get_xls_registry_value icc User`
ippassword=`get_xls_registry_value icc Password`

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start update ***********  \n"

workdir=$XLSDATA/bvd
# Name of the temp file to use
TMPFILE=${workdir}/direct_mapping_icc1.tmp

echo "\n remove any old temp files\n"
rm -f ${TMPFILE}

docopy=1
if [ "${docopy}" = 1 ]
then
timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start making temporary direct_mapping table ***********  \n"

echo "\n create copy of direct_mapping table to work with\n"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
select * into direct_mapping_new from direct_mapping
GO
HERE

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start creating indexes ***********  \n"

echo "\n creating indexes for temporary direct_mapping table\n"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create index direct_mapping01 on direct_mapping_new
    ( direct_mapping_type,  direct_mapping_value)
go
create index direct_mapping02 on direct_mapping_new
    ( source,  sourcekey)
go
HERE

fi

echo "\n delete icc_remove table if it exists"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table icc_remove
GO
HERE

echo "\n delete icc_alive table if it exists"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table icc_alive
GO
HERE

echo "\n delete icc_insert table if it exists"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table icc_insert
GO
HERE


###############################################

echo "\n bcp data from the remove table from icc database to a temporary file"
bcp remove out ${TMPFILE} -S ${ipserver} -U ${iplogin} -P ${ippassword}  /c /t"|" /eremoveout.err /m1000
if [ $? -ne 0 ]
then
	echo "Error in BCP out, exiting"
	exit 1
fi

echo "\n create icc_remove table"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table icc_remove (
	regd_no varchar(8) NOT NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating icc_remove table, exiting"
	exit 1
fi

echo "\n bcp in ids to remove"
bcp icc_remove in ${TMPFILE} -S ${xlsserver} -U ${xlslogin} -P ${xlspassword}  /c /t"|" /eremove.err /m1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi


timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start deleting records from temporary direct_mapping ***********  \n"

echo "\n delete records from temporary direct_mapping"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
delete from direct_mapping_new from icc_remove r where direct_mapping_new.sourcekey=r.regd_no and direct_mapping_new.source in(9972,9973)
GO
HERE

echo "\n delete icc_remove table"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table icc_remove
GO
HERE


##############################################

echo "\n remove any old temp files"
rm -f ${TMPFILE}

echo "\n bcp data from the alive table from icc database to a temporary file"
bcp alive out ${TMPFILE} -S ${ipserver} -U ${iplogin} -P ${ippassword}  /c /t"|" /ealiveout.err /m1000
if [ $? -ne 0 ]
then
	echo "Error in BCP out, exiting"
	exit 1
fi

echo "\n create icc_alive table"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table icc_alive (
	regd_no varchar(8) NOT NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating alive table, exiting"
	exit 1
fi

echo "\n bcp in ids to icc_alive"
bcp icc_alive in ${TMPFILE} -S ${xlsserver} -U ${xlslogin} -P ${xlspassword}  /c /t"|" /ealive.err /m1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

#exit 0

#######################################################################

echo "\n create temporary icc_insert table for ids that are not in direct mapping "

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n  Start creating temporary icc_insert table ***********  \n"

isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
select a.regd_no into icc_insert from icc_alive a left outer join direct_mapping_new d on a.regd_no=d.sourcekey and d.source=9972 where d.sourcekey is null
GO
HERE

echo "\n delete icc_alive table"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table icc_alive
GO
HERE

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start populating temporary direct_mapping table for source=9972 ***********  \n"

isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
insert into direct_mapping_new select 9972,regd_no,1,regd_no from icc_insert
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error populating direct_mapping  table, exiting"
	exit 1
fi

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n  Start populating temporary direct_mapping table for source=9973 ***********  \n"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
insert into direct_mapping_new select 9973,regd_no,1,regd_no from icc_insert
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error populating direct_mapping  table, exiting"
	exit 1
fi

echo "\n delete icc_insert table"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table icc_insert
GO
HERE


timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n Start renaming direct_mapping_new table into direct_mapping ***********  \n"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
exec commit_concordance_copy direct_mapping
GO
HERE

if [ $? -ne 0 ]
then
	echo "Unable to rename direct_mapping_new table into direct_mapping, exiting"
	exit 1
fi

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n delete old direct_mapping table ***********  \n"
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table direct_mapping_old
GO
HERE


##############################################

timestamp="`date +%a' '%b' '%e'  '%T' '%Z' '%Y`"
echo "\n ********* ${timestamp}\n update of direct_mapping table for icc has finished. ***********  \n"

exit 0

