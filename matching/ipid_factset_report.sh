# Script file to generate the IP matching reports for factset
# 1 = Output filename
# 2 = Server
# 3 = Login
# 4 = Password
if [ $# -lt 4 ]
then

    echo "Usage: ipid_factset_report.sh reportfile server login password"
    exit 1

fi

finalblatfile=$1
server=$2
login=$3
password=$4

LOADDIR=${XLS}/src/scripts/loading/lionshares

# Now build an email to send to security matching
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2

rm -f  ${finalblatfile} ${blatfiletmp1} ${blatfiletmp2}

echo "*** ipid_factset update info: *** " > ${finalblatfile}
echo "" >> ${finalblatfile}
echo "*** Companies added: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}

bcp "select distinct convert(varchar(16), FDS_ID), Type, Name, City, State, Postal_Code, ISO, Ultimate_Parent from ipid_factset where FDS_ID not in (select distinct sourcekey from company_map where source=222222) " queryout ${blatfiletmp1} /S ${server} /U ${login} /P ${password} /c /t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out company added, exiting ..."
	exit 1
fi
cat ${blatfiletmp1}  >> ${finalblatfile}

echo "" >> ${finalblatfile}
echo "*** Companies deleted: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}

bcp "select distinct m.sourcekey, convert(varchar(255), m.xlsid), c.name, cou.name from company_map m, company c, country cou where source=222222 and c.id=m.xlsid and cou.id=c.country and sourcekey not in (select distinct FDS_ID from ipid_factset) and ''!=isnull(sourcekey, '') " queryout ${blatfiletmp2} /S ${server} /U ${login} /P ${password} /c /t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out company added, exiting ..."
	exit 1
fi
cat ${blatfiletmp2}  >> ${finalblatfile}

exit 0
