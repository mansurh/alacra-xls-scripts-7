truncate table ipid_date_itext
go
insert into ipid_date_itext
select c.id, MAX(i.last_report)
from company c, ipid_itext i, company_map m
where
m.source=30
and
m.sourcekey=i.matchkey
and
c.id = m.xlsid
group by c.id
