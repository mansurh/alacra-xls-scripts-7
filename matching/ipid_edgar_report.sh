# Script file to generate the IP matching reports for edgar
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_edgar_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=edgar

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in edgar"
select * from ipid_edgar
where convert(int,CIK) not in (select sourcekey from company_map where source=36)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from edgar"
select x.id, x.name from company x, company_map m
where
x.id = m.xlsid and m.source=36
and
m.sourcekey not in (select convert(int,CIK) from ipid_edgar)

/* Optional report - new information available in database */

HERE
