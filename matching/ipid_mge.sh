# Script file to build the IP matching table for mge
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = mge Server
# 6 = mge Login
# 7 = mge Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_mge.sh ipid_server ipid_database ipid_login ipid_password mge_server mge_login mge_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=mge

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << ENDOfIsql
SET NOCOUNT ON
select distinct convert(varchar, c.CompanyId), c.Name, s.Ticker, s.Cusip, s.Isin, s.Sedol, s.Ric, s.Country from CompanyData c, SecurityData s where c.CompanyId = s.CompanyId and c.active=1

ENDOfIsql

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey char(9) NOT NULL,
	name char(100) NOT NULL,
	ticker char(15) NULL,
	cusip char(9) NULL,
	isin char(12) NULL,
	sedol char(7) NULL,
	ric char(15) NULL,
	country char(5) NULL
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - index the table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE2
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(isin)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(sedol)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(ric)
GO
HERE2
if [ $? -ne 0 ]
then
	echo "Error indexing table, exiting"
	exit 1
fi


# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
