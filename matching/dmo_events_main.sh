XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Arguments:
# 1 = ipid to match
if [ $# -lt 1 ]
then
	echo "Usage: dmo_events_main.sh ipname"
	exit 1
fi

ipname=$1
IPIDdatabase=concordance

IPIDserver=`get_xls_registry_value ${IPIDdatabase} Server`
IPIDlogin=`get_xls_registry_value ${IPIDdatabase} User`
IPIDpassword=`get_xls_registry_value ${IPIDdatabase} Password`

workdir=$XLSDATA/matching/${ipname}/
mkdir -p ${workdir}
cd ${workdir}

echo "Generate dmo events for ${ipname}"
$XLS/src/scripts/matching/dmo_events.sh ${ipname} ${IPIDdatabase} ${IPIDserver} ${IPIDlogin} ${IPIDpassword}
if [ $? -ne 0 ]
then
	echo "error generating dmo events for ${ipname}, exiting ..."
	exit 1
fi

exit 0
