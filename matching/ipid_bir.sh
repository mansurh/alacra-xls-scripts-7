# Parse the command line arguments.
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = bir Server
# 6 = bir Login
# 7 = bir Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_bir.sh IPID_server ipid_database ipid_login ipid_password bir_server bir_login bir_password"
	exit 1
fi


# Save the runstring parameters in local variables
IPID_server=$1
IPID_database=$2
IPID_logon=$3
IPID_password=$4
bir_server=$5
bir_logon=$6
bir_password=$7


# XLSDATA=C:/users/xls/tmp
LOADDIR=${XLS}/src/scripts/loading/bir
DATFILE=ipid_bir.dat


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${bir_server}"
echo ""

bcp "exec extract_ipid_data" queryout ${DATFILE} /U ${bir_logon} /P ${bir_password} /S ${bir_server} /c /t "|"
if [ $? != 0 ]
then

	echo "Error extracting ipid_bir data"
	exit 1

fi

echo "Dropping old ipid_bir data on ${IPID_server}"

isql /U${IPID_logon} /P${IPID_password} /S${IPID_server} /Q "truncate table ipid_bir" /b
if [ $? != 0 ]
then

	echo "Error truncating ipid_bir table"
	exit 1

fi

# drop the indices for the table
#echo ""
#echo "Dropping indices for ipid_bir for faster loading"
#echo ""

#
# Load all the new data...
#

# Load the ipid_bir table
echo "Loading the ipid_bir table" 

bcp ${IPID_database}..ipid_bir in ${DATFILE} /U${IPID_logon} /P${IPID_password} /S${IPID_server} /f${LOADDIR}/ipid_bir.fmt /m100 /eipid_bir.err
if [ $? != 0 ]
then

	echo "Error loading ipid_bir table"
	exit 1

fi

echo ""
echo "Updated company matching for BIR OK."
echo ""

exit 0
