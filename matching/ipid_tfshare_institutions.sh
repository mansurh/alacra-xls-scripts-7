# Script file to build the IP matching table for tfshare
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = tfshare Server
# 6 = tfshare Login
# 7 = tfshare Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_tfshare.sh ipid_server ipid_database ipid_login ipid_password tfshare_server tfshare_login tfshare_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=tfshare_institutions
mkdir -p $XLSDATA/matching/tfshare_institutions
cd $XLSDATA/matching/tfshare_institutions

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}

isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w1700 -n -h-1 >${TMPFILE1} << TUN
SET NOCOUNT ON
select distinct o.*,getdate()
from holdings h join security s on h.securityid=s.securityid 
join owner o on h.ownerid=o.ownerid 
where ownercode<>303
TUN

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}
# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
CREATE TABLE [dbo].${TABLENAME} (
	[OwnerID] [int] NULL ,
	[OwnerName] [varchar] (60)  NULL ,
	[OwnerCode] [int] NULL ,
	[Address1] [varchar] (50)  NULL ,
	[Address2] [varchar] (50)  NULL ,
	[Address3] [varchar] (50)  NULL ,
	[City] [varchar] (50)  NULL ,
	[CountryID] [int] NULL ,
	[StateCode] [varchar] (2)  NULL ,
	[ProvinceID] [int] NULL ,
	[Zip] [varchar] (16)  NULL ,
	[Phone] [varchar] (50)  NULL ,
	[Fax] [varchar] (50)  NULL ,
	[TotalEquityAssets] [decimal](15, 0) NULL ,
	[AssetFlag] [char] (1)  NULL ,
	[ParentOwnerID] [int] NULL ,
	[SecuritiesHeld] [decimal](15, 0) NULL ,
	[SecuritiesBought] [decimal](15, 0) NULL ,
	[SecuritiesSold] [decimal](15, 0) NULL ,
	[TurnoverValue] [float] NULL ,
	[TurnoverRating] [int] NULL ,
	[USRegionID] [int] NULL ,
	[StyleID] [varchar] (2)  NULL ,
	[Orientation] [char] (1)  NULL ,
	[URL] [varchar] (100)  NULL ,
	[ReportDate] [datetime] NULL ,
	[PriorReportDate] [datetime] NULL,
	[loadeddate] [datetime] NULL
) ON [PRIMARY]
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - create indices of the table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create index ${TABLENAME}_01 on ${TABLENAME}
	(OwnerID)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(OwnerName)
GO
HERE

# step 8 - cross reference the country names
#isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} < ${XLS}/src/scripts/matching/ipid_${ipname}_country.sql

# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} ${TMPFILE3}
