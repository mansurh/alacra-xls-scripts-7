IF EXISTS (SELECT * FROM sysobjects WHERE name='sp_CreateDirectMappingTable' and type='P')
DROP procedure sp_CreateDirectMappingTable
go

create procedure sp_CreateDirectMappingTable
as
	select regd_no, 1, regd_no from gz_name
	union
	select regd_no, 2, regd_no from dir_info where regd_no not in (select regd_no from gz_name) and coalesce(ac_type,0)!=0

go
