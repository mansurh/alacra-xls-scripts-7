IF EXISTS (SELECT * FROM sysobjects WHERE name='sp_CreateDirectMappingTable' and type='P')
DROP procedure sp_CreateDirectMappingTable
go


create procedure sp_CreateDirectMappingTable 
@Param           VARCHAR (50)=null
as
	select UKReg_number , 1, 'L'+ matchkey 
	from ipid_onesource 
		where country in ('ireland','united kingdom') 
		and UKReg_number is not null
go