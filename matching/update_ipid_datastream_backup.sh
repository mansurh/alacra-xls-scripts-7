# Script file to update the IP matching backup table for Datastream
# Created 4 January 2008 by Armen Galoustian


IPIDdatabase="concordance"
IPIDtable="ipid_datastream"
TODAY=`date +%Y%m%d`
DATADIR=$XLSDATA/matching/datastream

XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

IPIDserver=`get_xls_registry_value ${IPIDdatabase} server`
IPIDuser=`get_xls_registry_value ${IPIDdatabase} user`
IPIDpassword=`get_xls_registry_value ${IPIDdatabase} password`


# make sure our data directory exists
mkdir -p ${DATADIR}
check_return_code $? "Error creating data directory ${DATADIR}. Aborting" 1


cd ${DATADIR}
check_return_code $? "Error changing to data directory ${DATADIR}. Aborting" 1

#--------------------------------------------------------------------------------------#

echo "Start running script to update ${IPIDtable}_backup table"

isql -U ${IPIDuser} -P ${IPIDpassword} -S ${IPIDserver} -b -r -Q "delete from ${IPIDtable}_backup"
check_return_code $? "Error deleting all rows from ${IPIDtable}_backup table. Aborting" 1

isql -U ${IPIDuser} -P ${IPIDpassword} -S ${IPIDserver} -b -r -Q "insert into ${IPIDtable}_backup select * from ${IPIDtable}"
check_return_code $? "Error updating ${IPIDtable}_backup table. Aborting" 1

echo "End running script."

#--------------------------------------------------------------------------------------#

exit 0



