XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script file to refresh normalized_names table
# Created 12 December 2005 by John Willcox

#!/bin/sh

# 1 = IPID Server
# 2 = IPID Login
# 3 = IPID Password

ARGS=3
#if [ $# -ne $ARGS ]
#then
#	send_error "Incorrect call - should be: normalize.sh ipid_server ipid_login ipid_password"
#	exit 1
#fi

IPIDdatabase=concordance
#IPIDserver=$1
#IPIDlogin=$2
#IPIDpassword=$3
if [ $# -gt 0 ]
then
	IPIDserver=$1
else
	IPIDserver=`get_xls_registry_value ${IPIDdatabase} Server`
fi

IPIDlogin=`get_xls_registry_value ${IPIDdatabase} User`
IPIDpassword=`get_xls_registry_value ${IPIDdatabase} Password`

SCRIPTDIR=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/normalize

# E-mail error message to administrators@alacra.com

send_error()
{
	error=$1
	echo "${error} - exiting"
	errorFile=${SCRIPTDIR}/error.txt
	> $errorFile
	echo "${error}" >> $errorFile
	blat $errorFile -t "administrators@alacra.com" -s "normalize.sh script failed"
	rm $errorFile
}

# Generate text file with details of sources to be normalized

create_queries()
{
	weekday=$1
	column=$2
	query_file=normalize_${column}.txt
	> ${SQLfile}
	echo "set nocount on" >> ${SQLfile}
	echo "select ${column} from normalize_report" >> ${SQLfile}
	echo "where updateDay = '${weekday}'" >> ${SQLfile}
	echo "GO" >> ${SQLfile}
	isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} -o ${query_file}
	if [ $? != 0 ]
	then
		send_error "Problem pulling normalize_report table into ${query_file}"
		exit 1
	fi
}

# Format text files

format_file()
{
	query_file=$1
	query_file="normalize_${query_file}.txt"
	query_file_temp="${query_file}TEMP"
	# Remove hyphens from results
	sed -e '/---/d' < ${query_file} > ${query_file_temp}
	# Remove headers from results
	sed -e '/ipidTable/d' < ${query_file_temp} > ${query_file}
	sed -e '/sourceId/d' < ${query_file} > ${query_file_temp}
	sed -e '/matchkeyColumn/d' < ${query_file_temp} > ${query_file}
	sed -e '/nameColumn/d' < ${query_file} > ${query_file_temp}
	sed -e '/whereClause/d' < ${query_file_temp} > ${query_file}
	sed -e '/mutable/d' < ${query_file} > ${query_file_temp}
	# Remove blank lines and tabs
	> ${query_file}
	while read line_by_line
	do 
	      echo "@${line_by_line}@" >> ${query_file}
	done < ${query_file_temp}
	sed 's/@//g' < ${query_file} > ${query_file_temp}
	sed -e '/^$/ d' < ${query_file_temp} > ${query_file}
	rm ${query_file_temp}
	if [ $? != 0 ]
	then
		send_error "Problem formatting file ${query_file}. Temp file is ${query_file_temp}"
		exit 1
	fi
}

# Load arrays from results file

load_arrays()
{
	echo "Load Arrays"
	count=0
	while read line
	do
		ipidTable[${count}]=${line}
		count=`expr $count + 1`
	done < normalize_ipidTable.txt
	count=0
	while read line
	do
		sourceId[${count}]=${line}
		count=`expr $count + 1`
	done < normalize_sourceId.txt
	count=0
	while read line
	do
		matchkeyColumn[${count}]=${line}
		count=`expr $count + 1`
	done < normalize_matchkeyColumn.txt
	count=0
	while read line
	do
		nameColumn[${count}]=${line}
		count=`expr $count + 1`
	done < normalize_nameColumn.txt
	count=0
	while read line
	do
		if [[ "$line" == "NULL" ]]
		then
			whereClause[${count}]="WHERE 1=1"
		else
			whereClause[${count}]="WHERE ${line}"
		fi
		count=`expr $count + 1`
	done < normalize_whereClause.txt
	count=0
	while read line
	do
		if [[ $line == "y" ]]
		then
			whereClause[${count}]="${whereClause[${count}]}"
		else
			whereClause[${count}]="${whereClause[${count}]} AND ${matchkeyColumn[${count}]} not in (select id from normalize_name where sourceId = ${sourceId[${count}]})"
		fi		
		mutable[${count}]=${line}
		count=`expr $count + 1`
	done < normalize_mutable.txt
	if [ $? != 0 ]
	then
		send_error "Problem loading arrays"
		exit 1
	fi
}

# Recreate table

recreate_table()
{
	echo "Recreate Table"
	> ${SQLfile}
	echo "DROP TABLE normalize_name_temp" >> ${SQLfile}
	echo "CREATE TABLE normalize_name_temp (" >> ${SQLfile}
	echo "id varchar (255) not null," >> ${SQLfile}
	echo "sourceId int not null," >> ${SQLfile}
	echo "name varchar(255)" >> ${SQLfile}
	echo ")" >> ${SQLfile}
	echo "GO" >> ${SQLfile}
	isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile}
	if [ $? != 0 ]
	then
		send_error "Problem recreating table"
		exit 1
	fi	
}

# Populate normalize_name_temp table

normalize()
{
	ip=$1
	mCol=$2
	nCol=$3
	iTab=$4
	wClause=$5
	mutab=$6
	> ${SQLfile}
	echo "set nocount on" >> ${SQLfile}
	if [[ $mutab == "y" ]]
	then
		#echo "Deleting from normalize_name where sourceId=$ip"
		echo "DELETE FROM normalize_name" >> ${SQLfile}
		echo "WHERE sourceId=$ip" >> ${SQLfile}
		echo "GO" >> ${SQLfile}
	else
		#echo "Deleting from normalize_name where sourceId=$ip and id not in (select ${mCol} from ${iTab})"
		echo "DELETE FROM normalize_name" >> ${SQLfile}
		echo "WHERE sourceId=$ip" >> ${SQLfile}
		echo "AND id not in (select ${mCol} from ${iTab})" >> ${SQLfile}
		echo "GO" >> ${SQLfile}
	fi
	echo "INSERT INTO normalize_name_temp" >> ${SQLfile}
	echo "SELECT DISTINCT ${mCol}, ${ip}, UPPER(${nCol})" >> ${SQLfile}
	echo "FROM ${iTab}" >> ${SQLfile}
	echo "${wClause}" >> ${SQLfile}
	echo "GO" >> ${SQLfile}
	isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile}
	if [ $? != 0 ]
	then
		send_error "Problem populating normalize_name_temp table ip:$ip matchkeyColumn:$mCol nameColumn:$nCol ipidTable:$iTab whereClause:$wClause"
		exit 1
	fi
}

# Remove punctuation from normalize_name table

removePunctuation()
{
table=$1
nameColumn=$2
ip=$3
isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} << ENDSQL
set nocount on
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, ' ', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, ',', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, "'", '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, '.', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, '&', 'AND')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, '-', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, '(', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, ')', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, '@', '')
WHERE sourceId=$ip
GO
UPDATE ${table}
SET ${nameColumn} = REPLACE(${nameColumn}, '/', '')
WHERE sourceId=$ip
GO
ENDSQL
if [ $? != 0 ]
then
	send_error "Problem removing punctuation from ${nameColumn} in ${table}"
	exit 1
fi
}

# Normalize words using normalize_stopwords.txt file in ${SCRIPTDIR}

normalizeWords()
{
	table=$1
	nameColumn=$2
	ip=$3
	
	file_length=163
	file_length=`expr $file_length + 0`
	count=2

	> ${SQLfile}

	echo "set nocount on" >> ${SQLfile}

	while [ $count -lt $file_length ]
	do

	short_name=`head -n$count $stopwords_file | tail -1`
	count=`expr $count + 1`
	long_name=`head -n$count $stopwords_file | tail -1`

	echo "UPDATE ${table}" >> ${SQLfile}
	echo "SET ${nameColumn} = REPLACE(${nameColumn}, '${short_name}', '${long_name}') WHERE sourceId=$ip" >> ${SQLfile}

	count=`expr $count + 1`

	done

	echo "GO" >> ${SQLfile}

	isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile}
	if [ $? != 0 ]
	then
		send_error "Problem substituting stopwords"
		exit 1
	fi
}

# Select into normalize_name and drop normalize_name_temp

resolve_tables()
{
	echo "Resolve Tables"
	> ${SQLfile}
	echo "INSERT INTO normalize_name" >> ${SQLfile}
	echo "SELECT id, sourceId, name, getdate()" >> ${SQLfile}
	echo "FROM normalize_name_temp" >> ${SQLfile}
	#echo "DROP TABLE normalize_name_temp" >> ${SQLfile}
	echo "GO" >> ${SQLfile}
	isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile}
	if [ $? != 0 ]
	then
		send_error "Problem copying from normalize_name_temp to normalize_name"
		exit 1
	fi
}

# Create indices in normalize_name table

createIndex()
{
	table=$1
	column=$2
	
	echo "Creating index for ${column} in ${table}"
	
	> ${SQLfile}
	echo "DROP INDEX ${table}.${table}_${column}" >> ${SQLfile}
	echo "CREATE INDEX ${table}_${column} on ${table}" >> ${SQLfile}
	echo "(${column})" >> ${SQLfile}
	echo "GO" >> ${SQLfile}
	isql -n -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile}
	if [ $? != 0 ]
	then
		send_error "Problem creating index ${table}.${table}_${column} in ${table}"
		exit 1
	fi
}

# Remove temporary files

clean_up()
{
	rm normalize_ipidTable.txt
	rm normalize_sourceId.txt
	rm normalize_nameColumn.txt
	rm normalize_matchkeyColumn.txt
	rm normalize_whereClause.txt
	rm normalize_mutable.txt
	rm $SQLfile
}

mkdir -p ${DATADIR}
cd ${DATADIR}

stopwords_file=${SCRIPTDIR}/normalizeStopwords.txt

SQLfile=${DATADIR}/stopwordSQL.txt

DAY=`date +%a`

#set -A column ipidTable sourceId matchkeyColumn nameColumn whereClause mutable
colLst="ipidTable sourceId matchkeyColumn nameColumn whereClause mutable"
for i in $colLst
do
	create_queries "${DAY}" "${i}"
	format_file "$i"
done
load_arrays
recreate_table
i=0
MAX_ELEMENTS=${#sourceId[*]}
while [ $i -lt $MAX_ELEMENTS ]
do
	echo "Removing punctuation and normalizing ip ${sourceId[$i]}"
	normalize "${sourceId[$i]}" "${matchkeyColumn[$i]}" "${nameColumn[$i]}" "${ipidTable[$i]}" "${whereClause[$i]}" "${mutable[$i]}"
	removePunctuation "normalize_name_temp" "name" "${sourceId[$i]}"
	normalizeWords "normalize_name_temp" "name" "${sourceId[$i]}"
	i=`expr $i + 1`
done
resolve_tables
createIndex normalize_name sourceId
createIndex normalize_name id
createIndex normalize_name name
clean_up
echo "Done"