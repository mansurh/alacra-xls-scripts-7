. $XLS/src/scripts/loading/dba/get_registry_value.fn
. $XLS/src/scripts/loading/dba/check_return_code.fn
. $XLS/src/scripts/loading/dba/check_bcp_errors.fn

# Script file to build the IP matching table for IBISWorld

WD=$XLSDATA/matching

mkdir -p ${WD}
cd ${WD}

# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Password
# 4 = IBISWorld server
# 5 = IBISWorld login
# 6 = IBISWorld password

ARGS=7
if [ $# -ne $ARGS ]
then
    echo "Usage: ipid_ibisworld.sh ipid_server ipid_database ipid_login ipid_password ibisworld_server ibisworld_login ibisworld_password"
    exit 1
fi

IPID_SERVER=$1
IPID_DATABASE=$2
IPID_LOGIN=$3
IPID_PASS=$4
IBISWORLD_SERVER=$5
IBISWORLD_LOGIN=$6
IBISWORLD_PASS=$7

# Function to retrieve data from IBISWorld server

retrieve_data() {

    echo "Retrieving data from IBISWorld server ..."

    isql -S ${IBISWORLD_SERVER} -U ${IBISWORLD_LOGIN} -P ${IBISWORLD_PASS} -Q " drop table iworldcmp..ipid_ibisworld "
    isql -S ${IBISWORLD_SERVER} -U ${IBISWORLD_LOGIN} -P ${IBISWORLD_PASS} -Q " drop table iworldcmp..ipid_ibisworld_tmp "

    isql -U${IBISWORLD_LOGIN} -P${IBISWORLD_PASS} -S${IBISWORLD_SERVER} -b -Q " \
    select distinct d1.prop_value as 'matchkey', d1.prop_value as 'name', replace(d2.prop_value, 'AU', 'Australia') as 'country', d3.prop_value as 'anzsic', getdate() as 'last_update' \
    into ipid_ibisworld_tmp \
    from doc_props d1, doc_props d2, doc_props d3 \
    where \
        d1.doc_id = d2.doc_id and \
        d2.doc_id = d3.doc_id and \
        d1.prop_name = 'title' and \
        d2.prop_name = 'geo' and \
        d3.prop_name = 'ind_disp' "

    check_return_code $? "ISQL failed, building the new ipid table. Aborting" 1

    isql -U${IBISWORLD_LOGIN} -P${IBISWORLD_PASS} -S${IBISWORLD_SERVER} -b -Q " update ipid_ibisworld_tmp set matchkey = replace(matchkey, ' (Company Profile Report)', '') "
    isql -U${IBISWORLD_LOGIN} -P${IBISWORLD_PASS} -S${IBISWORLD_SERVER} -b -Q " update ipid_ibisworld_tmp set matchkey = replace(matchkey, ' (Company Premium Report)', '') "
    isql -U${IBISWORLD_LOGIN} -P${IBISWORLD_PASS} -S${IBISWORLD_SERVER} -b -Q " update ipid_ibisworld_tmp set name = replace(name, ' (Company Profile Report)', '') "
    isql -U${IBISWORLD_LOGIN} -P${IBISWORLD_PASS} -S${IBISWORLD_SERVER} -b -Q " update ipid_ibisworld_tmp set name = replace(name, ' (Company Premium Report)', '') "

    isql -S ${IBISWORLD_SERVER} -U ${IBISWORLD_LOGIN} -P ${IBISWORLD_PASS} -Q " select distinct * into ipid_ibisworld from ipid_ibisworld_tmp "

    bcp iworldcmp..ipid_ibisworld out ipid_ibisworld.dat -U ${IBISWORLD_LOGIN} -P ${IBISWORLD_PASS} -S ${IBISWORLD_SERVER} -c -m 5 -e ipid_ibisworld.err

    check_return_code $? "bcp failed outputting temporary bulk data. Aborting" 1
    check_bcp_errors ipid_ibisworld.err 10 "Failed to bcp out ipid_ibisworld table (too many errors). Aborting" 1

    isql -U${IPID_LOGIN} -P${IPID_PASS} -S${IPID_SERVER} -b -Q " truncate table concordance..ipid_ibisworld "

    bcp concordance..ipid_ibisworld in ipid_ibisworld.dat -U ${IPID_LOGIN} -P ${IPID_PASS} -S ${IPID_SERVER} -c -m 5 -e ipid_ibisworld.err

    check_return_code $? "bcp failed loading new bulk data. Aborting" 1
    check_bcp_errors ipid_ibisworld.err 10 "Failed to bcp in ipid_ibisworld table (too many errors). Aborting" 1

    echo "... done"
}

retrieve_data

exit 0
