# Script file to generate the IP matching reports for finex
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_finex.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=finex

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in finex"
select i.id,i.name from ipid_finex i
where i.id not in (select CONVERT(int,sourcekey) from company_map where source=7)
and i.parent is null and ticker is not null

/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from finex"
select x.id, x.name from company x, company_map m
where
x.id = m.xlsid and m.source=7
and
CONVERT (int,m.sourcekey) not in (select id from ipid_finex)

/* Optional report - new information available in database */
PRINT "Companies with new web pages"
select x.id, x.name, x.parent, i.web_address from company x, company_map m, ipid_finex i
where
x.id = m.xlsid and CONVERT(int,m.sourcekey) = i.id
and m.source=7
and (x.url is null or x.url = " ")
and i.web_address <> " "

/* Optional report - companies with name changes.  This is only */
/* valid for Finex where naming methodologies are similar. */
PRINT "Finex companies with names different than xls (sans J.P. Morgan)"
select x.id, x.parent, i.name "Finex Name", x.name "XLS Name" from
company x, company_map m, ipid_finex i
where
x.id = m.xlsid and m.source=7 and CONVERT (int,m.sourcekey) = i.id
and
x.name <> i.name
and
x.id <> 1003425
order by i.name

HERE
