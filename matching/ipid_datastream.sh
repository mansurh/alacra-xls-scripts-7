XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Script file to build the IP matching table for Datastream
# Created 1 September 2005 by John Willcox

#!/bin/sh

# 1 = IPID server
# 2 = IPID database
# 3 = IPID login
# 4 = IPID password

ARGS=4
if [ $# -ne $ARGS ]
then
	send_error "Incorrect call - should be: ipid_datastream.sh ipid_server ipid_database ipid_login ipid_password"
	exit 1
fi

IPIDname="datastream"
IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
#DATASTREAM_FTP=$5
#DATASTREAM_LOGIN=$6
#DATASTREAM_PW=$7
DATASTREAM_FTP=`get_xls_registry_value ${IPIDname} Server 6`
DATASTREAM_LOGIN=`get_xls_registry_value ${IPIDname} User 6`
DATASTREAM_PW=`get_xls_registry_value ${IPIDname} Password 6`

SCRIPTDIR=$XLS/src/scripts/matching
DATADIR=$XLSDATA/matching/${IPIDname}



# Send error message

send_error()
{
	error=$1
	echo "${error} - exiting" 
}

# Retrieve data from Datastream FTP server

retrieve_files()
{
	echo "Retrieving data file from Datastream FTP server" 
	> ${FTP_FILENAME}
	echo "user ${DATASTREAM_LOGIN}" >> ${FTP_FILENAME}
	echo "${DATASTREAM_PW}" >> ${FTP_FILENAME}
	echo "cd send" >> ${FTP_FILENAME}
	echo "binary" >> ${FTP_FILENAME}
	echo "prompt" >> ${FTP_FILENAME}	
	echo "get ${DATASTREAM_FILENAME}" >> ${FTP_FILENAME}
	echo "quit" >> ${FTP_FILENAME}
	ftp -i -n -s:${FTP_FILENAME} ${DATASTREAM_FTP}
	if [ $? != 0 ]
	then
		send_error "FTP retrieve file process failed"
		exit 1
	fi
	# Ensure output file exists
	if [ ! -e $DATASTREAM_FILENAME ]
	then
		send_error "Can't find ${DATASTREAM_FILENAME}"
		exit 1
	fi
}

# Remove quotes from file

remove_quotes()
{
	echo "Removing quotes from ${DATASTREAM_FILENAME} and outputting to ${EDITED_FILENAME}" 
	qcd2pipe < ${DATASTREAM_FILENAME} > ${EDITED_FILENAME}
	if [ $? != 0 ]
	then
		send_error "Problem removing quotes from ${DATASTREAM_FILENAME}"
		exit 1
	fi
}

# Drop existing ipid table

rename_ipid_tables()
{
	echo "Dropping ${IPID_TABLE}" 
	> $SQLfile
	echo "if (object_id('${IPID_TABLE}_backup','U') is not null) DROP TABLE ${IPID_TABLE}_backup" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "exec sp_rename '${IPID_TABLE}', '${IPID_TABLE}_backup'" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "exec sp_rename '${IPID_TABLE}_new', '${IPID_TABLE}'" >> $SQLfile
	echo "GO" >> $SQLfile
	isql -b -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} 
	if [ $? -ne 0 ]; then echo "error renaming into backup table"; exit 1; fi;
}

# Create new ipid table

create_new_table()
{
	echo "Creating ${IPID_TABLE}_new" 
	> $SQLfile
	echo "if (object_id('${IPID_TABLE}_new','U') is not null) DROP TABLE ${IPID_TABLE}_new" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "select top 0 * into ${IPID_TABLE}_new from ${IPID_TABLE}" >> $SQLfile
	echo "GO" >> $SQLfile
	isql -b -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} 
	if [ $? != 0 ]
	then
		send_error "Failed to create table"
		exit 1
	fi
}

# Bulk copy file into table

bulk_copy()
{
	echo "Bulk copying ${EDITED_FILENAME} into ${IPID_TABLE}_new" 
	bcp ${IPID_TABLE}_new in ${EDITED_FILENAME} /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /c /t"|" /b 5000 	
	if [ $? != 0 ]
	then
		send_error "BCP not completed"
		exit 1
	fi
}

# Add unadjusted price suffix where necessary

add_suffix()
{
	echo "Adding unadjusted price suffix to ${IPID_TABLE}_new" 
	> $SQLfile
	echo "update ${IPID_TABLE}_new" >> $SQLfile
	echo "set matchkey = matchkey+'(UP)'" >> $SQLfile
	echo "where price is null" >> $SQLfile
	echo "and unadjusted_price is not null" >> $SQLfile
	echo "go" >> $SQLfile
	isql -b -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} 
	if [ $? != 0 ]
	then
		send_error "Failed to add unadjusted price suffix"
		exit 1
	fi
}

# Format countries to match Alacra countries

format_countries()
{
	echo "Formatting countries to match Alacra countries" 
	> $SQLfile
	echo "exec update_datastream_countries" >> $SQLfile
	echo "GO" >> $SQLfile
	isql -b -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} 
	if [ $? != 0 ]
	then
		send_error "Problem formatting countries"
		exit 1
	fi
}

# Format exchanges to match Alacra exchanges

format_exchanges()
{
	echo "Formatting exchanes to match Alacra exchanges" 
	> $SQLfile
	echo "exec update_datastream_exchanges" >> $SQLfile
	echo "GO" >> $SQLfile
	isql -b -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} 
	if [ $? != 0 ]
	then
		send_error "Failed to format exchanges"
		exit 1
	fi
}

# Create indices

create_indices()
{
	echo "Creating indices" 
	> $SQLfile
	echo "create index ${IPID_TABLE}_01 on ${IPID_TABLE}_new" >> $SQLfile
	echo "(matchkey)" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "create index ${IPID_TABLE}_02 on ${IPID_TABLE}_new" >> $SQLfile
	echo "(name)" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "create index ${IPID_TABLE}_03 on ${IPID_TABLE}_new" >> $SQLfile
	echo "(sedol)" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "create index ${IPID_TABLE}_04 on ${IPID_TABLE}_new" >> $SQLfile
	echo "(isin)" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "create index ${IPID_TABLE}_05 on ${IPID_TABLE}_new" >> $SQLfile
	echo "(exchange)" >> $SQLfile
	echo "GO" >> $SQLfile
	echo "create index ${IPID_TABLE}_06 on ${IPID_TABLE}_new" >> $SQLfile
	echo "(domicile)" >> $SQLfile
	echo "GO" >> $SQLfile
	isql -b -U${IPIDlogin} -P${IPIDpassword} -S${IPIDserver} -i ${SQLfile} 	
	if [ $? != 0 ]
	then
		send_error "Failed to create indices"
		exit 1
	fi
}

# Clean up

clean_up()
{
	echo "Cleaning up" 
	rm -f ${DATASTREAM_FILENAME} ${EDITED_FILENAME} ${FTP_FILENAME} ${SQLfile}
	echo "Done" 
}

mkdir -p ${DATADIR}
cd ${DATADIR}

DATASTREAM_FILENAME="REFERENCE.AA.CSV"
EDITED_FILENAME="REFERENCE.CSV"
IPID_TABLE="ipid_${IPIDname}"
BCP_FILENAME="${SCRIPTDIR}/ipid_datastream.fmt"
FTP_FILENAME="${DATADIR}/datastream.ftp"
SQLfile=${DATADIR}/stopwordSQL.txt

clean_up
retrieve_files
remove_quotes
create_new_table
bulk_copy
add_suffix
format_countries
format_exchanges
create_indices
rename_ipid_tables
