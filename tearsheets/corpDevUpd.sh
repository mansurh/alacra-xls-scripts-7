isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=10\
 updatetext xls..tearsheet.contents @ptr 0 NULL \
'TearSheet Frame outer_shell;\
TearSheet Frame inner_shell;\
TearSheet Label header;\
header setStyle TITLE ;\
header setText 	\"Bristol-Myers Corporate Development\";\
TearSheet Label sources; \
sources setStyle FOOTER ; \
sources setText \"Sources include Nelson Information, Inc. and Media General Financial Services";\
TearSheet Label footer; \
footer setStyle FOOTER ; \
footer setText \"Copyright, 1998 Data Downlink Corporation\";\
SS columnWidth 0 32;\
SS columnWidth 1 20;\
SS columnWidth 2 20;\
SS columnWidth 3 20;\
SS columnWidth 4 20;\
SS columnWidth 5 20;\
SS columnWidth 6 20;\
SS columnWidth 7 20;\
TearSheet NelsonProfile profile; \
profile switchField shares off; \
profile switchField industry off; \
profile switchField marketCap off; \
profile switchField following off; \
profile switchField currency off; \
'"

isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=10\
 updatetext xls..tearsheet.contents @ptr NULL 0 \
'\
TearSheet NelsonExec keyExecs; 	\
TearSheet NelsonEarn earnings; 	\
TearSheet NelsonAAL AAL; 	\
TearSheet MGFSBusSeg busSegs;\
TearSheet MGFSStats stats;\
TearSheet MGFSQuarter qPerf;\
TearSheet MGFSYear yPerf;\
TearSheet MGFSRatios ratios;\
TearSheet MGFSIndPct indPct;\
TearSheet Label indPctHeader;\
indPctHeader setStyle TITLE ;\
indPctHeader setText \"Company as % of Industry\";\
AAL switchField Legal_Counsel off;\
outer_shell add header 0 0;\
inner_shell add profile header 0; \
inner_shell add keyExecs 0 profile; \
'"

isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=10\
 updatetext xls..tearsheet.contents @ptr NULL 0 \
'\
inner_shell add AAL keyExecs+1 profile;\
inner_shell add earnings AAL+1 profile;\
inner_shell add busSegs earnings+1 0;\
inner_shell add stats busSegs+1 0;\
inner_shell add qPerf stats+1 0;\
inner_shell add yPerf qPerf+1 0;\
inner_shell add ratios yPerf+1 0;\
inner_shell add indPctHeader ratios+1 0;\
inner_shell add indPct indPctHeader+1 0;\
outer_shell add inner_shell header 0;\
outer_shell add sources inner_shell 0;\
outer_shell add footer sources 0;\
outer_shell break;\
foreach company \$companyIDs {\
profile setCompany \$company ; \
keyExecs setCompany \$company ; \
AAL setCompany \$company ; \
earnings setCompany \$company ; \
busSegs setCompany \$company ; \
stats setCompany \$company ; \
qPerf setCompany \$company ; \
yPerf setCompany \$company ; \
ratios setCompany \$company ; \
indPct setCompany \$company ; \
Render outer_shell [SS maxRow] 0; \
};\
SS output; '"



