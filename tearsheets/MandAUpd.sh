isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=13\
 updatetext xls..tearsheet.contents @ptr 0 NULL \
'TearSheet Frame outer_shell;\
TearSheet Frame inner_shell;\
TearSheet Label header;\
header setStyle TITLE ;\
header setText \"Corporate Development: M & A\";\
TearSheet Label footer;\
footer setStyle FOOTER ;\
footer setText \"Copyright, 1998 Data Downlink Corp\";\
TearSheet Label sources;\
sources setStyle FOOTER ;\
sources setText \"Sources include Mergerstat\";\
TearSheet MergStat ma;\
inner_shell add ma header 0;\
'"

isql /S$1 /Uxls /Pxls /Q "declare @ptr  varbinary(16)\
 select @ptr = textptr(contents) from xls..tearsheet\
 where id=13 \
updatetext xls..tearsheet.contents @ptr NULL 0 '\
outer_shell add header 0 0;\
outer_shell add inner_shell header 0;\
outer_shell add sources inner_shell 0;\
outer_shell add footer sources 0;\
outer_shell break;\
SS columnWidth 0 12;\
SS columnWidth 1 32;\
SS columnWidth 2 32;\
SS columnWidth 3 32;\
SS columnWidth 4 32;\
SS columnWidth 5 12;\
SS columnWidth 6 12;\
SS columnWidth 7 16;\
SS columnWidth 8 32;\
foreach company \$companyIDs {\
ma setCompany \$company;\
Render outer_shell [SS maxRow] 0;};\
SS output;'"

