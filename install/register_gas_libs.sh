cd c:/alacra/lib
if [ $? -ne 0 ]
then
	echo "error cd to c:/alacra/lib"
	exit 1
fi

for file in `ls *.dll`
do
  echo "registering $file"
  ../gacutil.exe /i $file
done
