# Command file to update a custom alacra book on a web server
# Parse the command line arguments.
#	1 = user
#   2 = ftpserver
#	3 = ftpbasedocsdir
#   4 = ftpuser
#   5 = ftppassword
shname=updatebook.sh
if [ $# -lt 5 ]
then
	echo "Usage: ${shname} user ftpserver ftpbasedocsdir ftpuser ftppassword"
	exit 1
fi

# Set up vars
user=$1
ftpserver=$2
data6docsdir=$3
ftpuser=$4
ftppassword=$5

todaysdate=$(date +%y%m%d)

installsemail="installs@alacra.com"
logfilename=updatebook_${user}_${todaysdate}.log

# work in root dir
cd c:/

$XLS/src/scripts/install/updatebooklow.sh ${user} ${ftpserver} ${data6docsdir} ${ftpuser} ${ftppassword} > ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators
if [ ${returncode} != 0 ]
then
	echo "${shname}: Bad return code from update.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "${installsemail}" -s "Book ${user} Update Failed on $COMPUTERNAME"
else
	# good return code, mark update complete in DBA database
	blat ${logfilename} -t "${installsemail}" -s "Book ${user} Updated Successfully on $COMPUTERNAME"
fi

rm -f ${logfilename}

exit $returncode
