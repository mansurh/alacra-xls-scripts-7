XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]
then
	echo "usage: $0 dbname [path] [server] [instance]"
	exit 1
fi
dbname=$1

sqlbackupdir=.
if [ $# -gt 1 ]
then
  sqlbackupdir=$2
fi

if [ $# -gt 2 ]
then
  server=$3
  #get instance name, if not blank, and add it to server
  if [ "$4" != "" ]; then server="$server\\$4"; fi;
else
  server=`get_xls_registry_value ${dbname} Server`
fi


backupdir=backups

user=`get_xls_registry_value ${dbname} User`
pass=`get_xls_registry_value ${dbname} Password`

#substitute "$" with "`$" pattern to work around powershell's handling of $ char
escpass=${pass//\$/\`$}


main() {
date
mkdir -p ${sqlbackupdir}
cd ${sqlbackupdir}
if [ $? -ne 0 ]
then
	echo "error cd to ${backupdir}, exiting ..."
	exit 2
fi

if [ `date +%d` == "01" ]
then
  #create a monthly backup on the first of the month
  filename=${dbname}.`date +%b`.zip
elif [ `date +%w` -eq 6 ]
then
  #create a weekly file for 5 weeks on Saturdays
  typeset -i weeknum
  weeknum=0x`date +%V`%5
  filename=${dbname}.W${weeknum}.zip
else
  #create a copy for day of the week
  filename=${dbname}.`date +%w`.zip
fi

#remove old version of the database
rm -rf ${dbname}*
powershell $XLS/schema/generate_schema.ps1 ${server} ${dbname} ${user} ${escpass} 
if [ $? -ne 0 ]; then echo "error in generate_schema.ps1, exiting ..."; exit 1; fi

#get a list of tables which we don't want to backup
excl_list="${dbname}/excl_tables.txt"
rm -f ${excl_list}
bcp "select tablename from exclude_backup_table_list" queryout ${excl_list} /S${server} /U${user} /P${pass} /c
touch excl_tables.txt 

declare -A arr;

while read table
do
  arr[$table]=1

done < ${excl_list}

for table in `ls ${dbname}/tables`
do
  date
  if [ -n "${arr[$table]}" ]
  then
    echo "skipping backup for table: $table"
    continue;
  fi;
  tempfile=./${dbname}/tables/${table}/data.bcp
  echo $tempfile  
  $XLS/src/scripts/transfer/dumpdataonly.sh ${table} ${dbname} ${server} ${user} ${pass} ${tempfile}
  if [ $? -ne 0 ]; then echo "error in dumping data for $table, exiting ..."; exit 1; fi
  #move & compress bcp file into zip file with -m option
  zip -m ${tempfile}.zip ${tempfile}
  if [ $? -ne 0 ]; then echo "error in zip of $tempfile, exiting ..."; exit 1; fi
  
done

date

echo "zipping backup"
zip -rm ${filename} ${dbname}
if [ $? -ne 0 ]
then
	echo "error in tar of ${filename} file, exiting ..."
	exit 1
fi
date

chmod a+r ${filename}
rm -f nul
date
echo "Moving backup file"
mkdir -p $backupdir
rm -f $backupdir/${filename} 
mv -f ${filename} $backupdir
if [ $? -ne 0 ]; then echo "error in moving ${filename} to $backupdir, exiting ..."; return 1; fi;

date
echo "Backup complete"
}

mkdir -p $XLSDATA/${dbname}

logfile=$XLSDATA/${dbname}/backup`date +%Y%m%d`.log
(main) > $logfile 2>&1
if [ $? -ne 0 ]
then
  blat ${logfile} -t administrators@alacra.com -s "Backup failed for $dbname on $server"
  exit 1
fi
