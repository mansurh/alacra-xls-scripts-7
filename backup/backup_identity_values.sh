XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]; then echo "usage: $0 database"; exit 1; fi;
database=$1

server=`get_xls_registry_value $database server`
backup=`get_xls_registry_value $database backup`
user=`get_xls_registry_value $database user`
password=`get_xls_registry_value $database password`

create_file="create_identity_statements.sql"
identity_file="new_identity_values.sql"
run () {
 rm -f ${create_file} ${identity_file}
 echo "Server: ${server}"
 echo "Backup: ${backup}"
 if [ "${server}" = "" -o "${backup}" = "" ]
 then
   echo "Empty server or backup values, exiting ..."; return 1;
 elif [ "${server}" = "${backup}" ]
 then
   echo "Server and Backup values are the same, exiting..."; return 1;
 fi
 echo "Generating ${create_file} file"
 isql /b /r /w 2000 /h-1 /n /S$server /U$user /P$password /Q "exec generate_identity_values_for_repl_tables" /o create_identity_statements.sql
 if [ $? -ne 0 ]; then echo "Error in generate_identity_values_for_repl_tables, exiting ..."; return 1; fi

 ls -l ${create_file} 
 if [ ! -s ${create_file} ]; then echo "${create_file} is empty, exiting ..."; return 1; fi;

 echo "Generating ${identity_file} file"
 isql /b /r /w 2000 /h-1 /n /S$server /U$user /P$password /i ${create_file} /o ${identity_file}
 if [ $? -ne 0 ]; then echo "Error running ${create_file} , exiting ..."; return 1; fi

 ls -l ${identity_file} 
 if [ ! -s ${identity_file} ]; then echo "${identity_file} is empty, exiting ..."; return 1; fi;

 echo "Running ${identity_file} against backup server: $backup";
 isql /b /r /w 2000 /h-1 /n /S$backup /U$user /P$password /i ${identity_file}
 if [ $? -ne 0 ]; then echo "Error running ${identity_file}, exiting ..."; return 1; fi

 echo "All Done!"
}

mkdir -p $XLSDATA/backup/log
cd $XLSDATA/backup
logfile=$XLSDATA/backup/log/copy_identity_values_${database}_`date +%y%m%d`.log
(run) > $logfile 2>&1
if [ $? -ne 0 ]
then
   blat $logfile -t administrators@alacra.com -s "Copy of identity values failed for $database"
   exit 1
fi
