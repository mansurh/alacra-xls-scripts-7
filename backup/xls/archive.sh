XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

let monthsback=36
if [ $# -gt 0 ]
then
  monthsback=$1
fi

DB=xlsdbo
user=`get_xls_registry_value ${DB} user`
pass=`get_xls_registry_value ${DB} password`
server=`get_xls_registry_value ${DB} server`

main() {
  date
  echo "Archive xls data on $server"
  isql /b /U${user} /P${pass} /S${server} /Q "exec moveArchiveInfo $monthsback"
  if [ $? -ne 0 ]; then echo "error in moveArchiveInfo"; return 1; fi
  date
}

mkdir -p $XLSDATA/xls
logfile=$XLSDATA/xls/archive`date +%y%m%d`.log
main > ${logfile} 2>&1
if [ $? -ne 0 ]
then
  blat $logfile -t administrators@alacra.com -s "Xls archive failed on ${server}"
  exit 1
fi