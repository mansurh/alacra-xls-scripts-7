echo "File-C Backup Started on Data5" > logfile
date >> logfile

ntbackup backup "f:\\markintel" /b /hc:on /t normal /l "logfile" /e /tape:0

echo "File-C Backup on Data5 Stopped" >> logfile
date >> logfile

# Mail Results of backup to Administrators group
blat "logfile" -t "administrators@xls.com" -s "File-C Backup on Data5 Log"
