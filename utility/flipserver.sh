#!/bin/ksh

# take in server name whose databases need to be flipped to their backups.
export server=$1

# isql requires:
ISQL='isql /Usa /Pnewpass /Sdata2'

$ISQL << EOF

use jsched
go

update dataserver 
set dataserver.flipped_server = a.server,
    dataserver.server = a.backup,
    dataserver.backup = " "
from dataserver, dataserver a
where dataserver.server = '$server'
and dataserver.dbname = a.dbname
go

EOF
