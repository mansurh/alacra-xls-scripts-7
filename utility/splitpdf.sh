TITLEBAR="split pdf file into pages"

# Parse the command line arguments.
#	1 = pdf file path
#	2 = output path

shname=$0
if [ $# -lt 2 ]
then
    echo "Usage: ${shname} pdffile outputpath"
    exit 1
fi

pdffile=$1
outpath=$2

if [ ! -d ${outpath} ]
then
    mkdir -p ${outpath}
fi

cd ${outpath}
if [ $? -ne 0 ]
then
	echo "${shname}: error changing directory to ${outpath}, exiting ..."
	exit 1
fi

if [ ! -s ${pdffile} ]
then
    echo "${shname}: ${pdffile} not found."
    exit 1
fi

filename="${pdffile##*\\}"
filename="${filename##*/}"
prefix="${filename%.pdf}"

# check if text file already exists
if [ -s "$prefix.txt" ]
then
	# check if the pdf file is newer.  if not, we're done
	if [ "$prefix.txt" -nt "$pdffile" ]
	then
#		echo "${shname}: $prefix.txt already exists."
		exit 0
	fi
fi

#####################################
### retrieve page number from pdf ###
#####################################
size="$(pdls $pdffile | grep -i ^page | wc -l | sed 's/[^[:digit:]]//g')"
#echo "${shname}: split $pdffile into $size pages"

if [ $size -lt 1 ]
then
    echo "${shname}: no page found in ${pdffile}"
    exit 1
fi

####################################
### split pdf into indivual page ###
####################################

# first output the entire text file
pdw $pdffile > $prefix.txt.tmp
if [ $? != 0 ]
then
    echo "${shname}: Error converting pdf to text, exiting."
    exit 1
fi

pageindex="index.$prefix.txt.tmp"
# build the page index file
if [ -e "$pageindex" ]
then
	rm -f -- "$pageindex"
fi

if [ -e "$prefix.txt.tmp" ]
then
	awk -f $XLS/src/scripts/utility/pageIndex.awk -- "$prefix.txt.tmp" > $prefix.txt.tmp2
	rm -f -- "$prefix.txt.tmp"
fi

if [ -e "$prefix.txt.tmp2" ]
then
	cat -- "$pageindex" "$prefix.txt.tmp2" > $prefix.txt
	rm -f -- "$prefix.txt.tmp2"
fi

if [ -e "$pageindex" ]
then
	rm -f -- "$pageindex"
fi

#echo "${shname}: splitting $filename into $size pages done"

exit 0
