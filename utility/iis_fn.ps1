
# create_iis_app
#
#  Example usage:
#
#	create_iis_app "MyApp" "asp\MyApp"

function create_iis_app
	{
	param([string] $appName,
		[string] $appPath,
		[int] $appIsolation = 2,
		[int] $readAccess = 1,
		[int] $scriptAccess = 1,
		[int] $execAccess = 1,
		[string] $defaultDoc = "Default.aspx",
		[bool] $requireHttps = $false,
		[string] $dotNetVersion = "2.0.50727")

		$app = [ADSI]"IIS://localhost/W3SVC/1/ROOT/$appPath"
		if (!$app.AdsPath) {
			$path = [ADSI]"IIS://localhost/W3SVC/1/ROOT"
			$app = $path.Create("IIsWebVirtualDir", $appPath)
		}
		$app.Put("AccessRead", $readAccess)
		$app.Put("AccessScript", $scriptAccess)
		$app.Put("AccessExecute", $execAccess)
		$app.Put("AppIsolated", $appIsolation)
		$app.Put("DefaultDoc", $defaultDoc)
		$app.Put("AccessSSL", $requireHttps)

		if ($dotNetVersion.CompareTo("4.0") -eq 0) {
			$app.AppCreate3(1,"dotNET4AppPool", 1)
		} else {
			$app.AppCreate2(1)
		}
		$app.Put("AppFriendlyName", $appName)
		$versionPath = $dotNetVersion
		if ($dotNetVersion.CompareTo("1.1") -eq 0) {
			$versionPath = "1.1.4322"
		} elseif ($dotNetVersion.CompareTo("2.0") -eq 0) {
			$versionPath = "2.0.50727"
		} elseif ($dotNetVersion.CompareTo("4.0") -eq 0) {
			$versionPath = "4.0.30319"
		}

		for ($i=0; $i -lt $app.ScriptMaps.Count; ++$i){
			$istart=$app.ScriptMaps[$i].indexOf("k\v");
			$iend=$app.ScriptMaps[$i].indexOf("\aspnet_isapi.dll");
			if ($istart -ge 0 -and $iend -ge $istart) {
				$cv = $app.ScriptMaps[$i].Substring($istart+3,($iend-$istart-3));
				$app.ScriptMaps[$i] = $app.ScriptMaps[$i].replace($cv,$versionPath);
			}
		}

		$app.SetInfo()
	}
