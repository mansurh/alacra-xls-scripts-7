# Parse the command line arguments.
#	1 = invfilesetname
#	2 = srcdir
#	3 = destdir [optional]
shname=moveInvFileSet.sh
if [ $# -lt 2 ]
then
	echo "Usage: ${shname} invfilesetname srcdir [destdir] [dbname]"
	exit 1
fi

#sleep for sometime (to make sure GIS lets go of the inverted index files)
sleep 30

invfilesetname=$1
srcdir=$2
destdir=${srcdir}/backup
if [ $# -gt 2 ]
then
	destdir=$3
fi

dbname=${invfilesetname}
if [ $# -gt 3 ]
then
	dbname=$4
fi


if [ ! -d ${srcdir} ]
then
	echo "${shname}: ${srcdir} is not a directory."
	exit 1
fi

	gisget.exe ${COMPUTERNAME} "topic=_lockfileset&regkey=${invfilesetname}"
	gisget.exe ${COMPUTERNAME} -p 4888 "topic=_lockfileset&regkey=${invfilesetname}"
	# tell GIS it's OK to use the index fileset (do this here, so if rebuild fails, we don't issue another lock command)
	gisget.exe ${COMPUTERNAME} "topic=_unlockfileset&regkey=${invfilesetname}"
	gisget.exe ${COMPUTERNAME} -p 4888 "topic=_unlockfileset&regkey=${invfilesetname}"

# other assorted detrius
rm -f ${srcdir}/__db.*
if [ $? -ne 0 ]
then
	#sleep for a bit, if delete failed.
	sleep 60s
	# make certain the GIS closes the inverted fileset

	rm -f ${srcdir}/__db.*
	if [ $? -ne 0 ]
	then
		echo "$0: Error: Could not removed __db.*"
		exit 1
	fi
fi

rm -f ${srcdir}/*.vct ${srcdir}/__db_lock.share



mkdir -p ${destdir}
if [ ! -d ${destdir} ]
then
	echo "${shname}: ${destdir} is not a directory."
	exit 1
fi


num_dbs=`ls -1 ${srcdir}/${invfilesetname}*.cfg | wc -l`

if [ ${num_dbs} -gt 1 ]
then

    echo "${shname}: another database with a name similar to '${invfilesetname}' has its index in the directory ${srcdir}"
    exit 1

fi


# most filenames start with the inverted fileset name
biglist="$(ls ${srcdir}/${invfilesetname}*)"
# except sitemap files which don't
if [ "${invfilesetname}" != "${dbname}" ]
then
	biglist2="$(ls ${srcdir}/${dbname}_${invfilesetname}*)"
	if [ "${biglist2}" != "" ]
	then
		biglist="${biglist} ${biglist2}"
	fi
fi

#echo "${biglist}"


for i in $biglist
do
	if [ -e ${i} ]
	then
		nameonly="${i##*/}"
		echo "${shname}: Moving ${i} to ${destdir}"
		mv -f ${i} ${destdir}/${nameonly}
		if [ $? != 0 ]
		then
			echo "${shname}: Error: Could not move ${i} to ${destdir}"
			exit 1
		fi
	fi
done


if [ -e $XLS/src/cfgfiles/${invfilesetname}.cfg ]
then
	sourceforcopy=$XLS/src/cfgfiles/${invfilesetname}.cfg
else
	sourceforcopy=${destdir}/${invfilesetname}.cfg
fi

echo "Copying ${sourceforcopy} to ${srcdir}/${invfilesetname}.cfg"
cp -p ${sourceforcopy} ${srcdir}/${invfilesetname}.cfg
if [ $? != 0 ]
then
	echo "${shname}: Error: Could not copy ${sourceforcopy} to ${srcdir}/${invfilesetname}.cfg"
	exit 1
fi

rm -rf ${srcdir}/temp

exit 0
