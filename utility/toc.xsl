<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >


<xsl:template match="p">
    <p>
        <xsl:value-of select="." />
    </p>
</xsl:template>


<xsl:template match="table">
    <table>
        <xsl:apply-templates select="*" />
    </table>
</xsl:template>


<xsl:template match="thead">
    <thead>
        <xsl:apply-templates select="*" />
    </thead>
</xsl:template>


<xsl:template match="tfoot">
    <tfoot>
        <xsl:apply-templates select="*" />
    </tfoot>
</xsl:template>


<xsl:template match="tbody">
    <tbody>
        <xsl:apply-templates select="*" />
    </tbody>
</xsl:template>


<xsl:template match="tr">
    <tr>
        <xsl:apply-templates select="*" />
    </tr>
</xsl:template>


<xsl:template match="th">
    <th>
        <xsl:value-of select="." />
    </th>
</xsl:template>


<xsl:template match="td">
    <td>
        <xsl:value-of select="." />
    </td>
</xsl:template>


<!--
ToDo:  Support col-groups?
-->


<xsl:template match="head">
</xsl:template>


<xsl:template match="/">

    <html>
        <xsl:apply-templates select="*" />
    </html>

</xsl:template>


</xsl:stylesheet>
