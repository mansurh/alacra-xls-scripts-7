scriptname=$0
echo "DO NOT RUN WITHOUT PERMISSION FROM Information Systems personnel!!"
grep -q "^at " ${scriptname}
if [ $? -eq 0 ]
then
  echo "ERROR: Old command (at.exe) is used, no longer supported"
  echo "ERROR: No action taken, review the changes, and switch to SCHTASKS command"
  exit 1
fi

# Define the scheduling server
schedserver=EUDATA6
if [ $COMPUTERNAME != $schedserver ]
then
    echo "Not $schedserver, exiting..."
    exit 1
fi

run()
{

#catch if any of these fail
set -ex
#SCHTASKS /F /Delete /TN "Alacra *"


# Backup of xls db
SCHTASKS /Create /F /SC DAILY /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/XlsBackup" /TN "Alacra XlsBackup - EU" /RU ""
SCHTASKS /Create /F /SC HOURLY /MO 2 /st 01:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/XlsBackup_incr" /TN "Alacra XlsBackup_incr - EU" /RU ""

# Extract data from ACE and put it in concordance
# SCHTASKS /Create /F /SC DAILY /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_extractToConcordance"  /TN "Alacra ace_extractToConcordance - EU" /RU ""

# Daily alacralog check
#SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 01:23 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/alacralogcheck" /TN "Alacra alacralogcheck" /RU ""

# wcheck load
SCHTASKS /Create /F /SC DAILY /ST 01:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wcheck0_eu" /TN "Alacra wcheck update - Europe" /RU ""

# wcheck monthly refresh
SCHTASKS /Create /F /SC MONTHLY /MO FIRST /d SUN /st 05:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/wcheck_full_eu" /TN "Alacra wcheck_full - Europe" /RU ""

# Backup Compliance Databases (Wellsfargo,Barclays_corp,bankofamerica,nomura,ace dbs)
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/backup_compliance_dbs" /TN "Alacra backup_compliance_dbs - EU" /RU ""

#RDC Full refresh
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcfull_eu" /TN "Alacra rdcfull Europe" /RU ""

#RDC Incremental - This should run every day except first day, but no way to tell the scheduler, for now, ignore errors on first day
SCHTASKS /Create /F /SC DAILY /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rdcincremental_eu" /TN "Alacra rdcincremental Europe" /RU ""

#Dow Jones Watchlist Full refresh
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 08:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/djwatchlistFull_eu" /TN "Alacra djwatchlistFull Europe" /RU ""

#Dow Jones Watchlist Incremental update for previous day
SCHTASKS /Create /F /SC DAILY /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/djwatchlist_eu" /TN "Alacra djwatchlist Europe" /RU ""

# CZB RDCGrid Daily load
#SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/czb_rdcgrid_load"  /TN "Alacra czb_rdcgrid - EU" /RU ""

#KYC FILE IMPORT - EU servers
SCHTASKS /Create /F /SC Weekly /D SUN /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/kyc_eu"  /TN "Alacra KYC Data load - EU" /RU ""

# ACMS failure notification
#SCHTASKS /Create /F /SC DAILY /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/acms_autoresubmit" /TN "Alacra ACMS failure notification - EU" /RU ""

# Dow Jones monitored relationship count
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 00:25 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/dow_jones_relationship_count_eu" /TN "Alacra Dow Jones monitored relationship count - EU" /RU ""

# Archive ACE monitoring results
SCHTASKS /Create /F /SC Weekly /D SAT /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_archive_monitor_results_eu"  /TN "Alacra Archive ACE monitoring results - EU" /RU ""

#ICBC load
SCHTASKS /Create /F /SC DAILY /ST 16:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/icbc_load" /TN "Alacra ICBC load - EU" /RU ""

# Run Compliance Batch locker clean-up utility
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/compbatch_locker_cleanup_eu" /TN "Alacra compbatch_locker_cleanup" /RU ""

# Run BRIT Insurance auto export
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 04:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/ace_export_230550" /TN "Alacra DJ BRIT Insurance auto export - EU" /RU ""

}

rm -f *.log
logfile=atlist`date +%m%d`.log
echo "Logfile is: $logfile"
(run) > $logfile 2>&1
rc=$?

#Get Number of jobs listed in this file, and the number loaded into scheduler
typeset -i loaded
typeset -i listed
loaded=`SCHTASKS /query /NH |grep -v "^$"|grep Alacra|wc -l`
listed=`egrep -i "^(SCHTASKS /Create|at) " ${scriptname}|grep -iv " /delete"|wc -l`

if [ $rc -ne 0 ]
then
  #SCHTASKS /F /Delete /TN "Alacra *" > /dev/null
  echo
  echo
  echo
  echo "LAST JOB FAILED TO LOAD.  LOADED ONLY $loaded JOBS OUT OF $listed LISTED IN ${scriptname}"
  echo "TAKE A LOOK AT ITS CALL, AND TRY TO FIX IT"
  echo "Log file name is $logfile"
elif [ $loaded -ne $listed ]
then
  #If number of jobs loaded is different from listed, report a problem
  #This could happen if the same job is listed more than once for SCHTASKS
  echo
  echo
  echo "POSSIBLE ERRORS IN LOADING JOBS"
  echo "Number of jobs loaded ($loaded) mismatched number of listed jobs ($listed)."
  echo "All other jobs are scheduled to run, please review the difference for possible problems."
  echo "Log file name is $logfile"
else
  echo "$loaded jobs loaded successfully"
  echo "Do you want to inspect the log file?(Y/N)"
  read zzz
  if [ "$zzz" = "y" -o "$zzz" = "Y" ]
  then
    echo "Log file name is $logfile"
  else
    rm -f $logfile
  fi
fi

exit 0
