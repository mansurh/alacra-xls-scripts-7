XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn


#USAGE: GenNomuraAEJfile.sh <days back>


#######GET INPUTS################
mode=$1

if [ $mode == "range" ] 
then
    #computed parameters
    startdate=$2
    enddate=$3

elif [ $mode == "daysback" ]
then
    #computed parameters
    daysback=$2
    startdate=$(date --date="${daysback} day ago" +"%Y-%m-%d")
    enddate=$(date +"%Y-%m-%d")
else
    echo "USAGE: GenNomuraAEJfile.sh range <start date> <end date>"
    echo "       GenNomuraAEJfile.sh daysback <number  of days back>"
    echo "Date Format: yyyy-mm-dd"
    exit -1
fi


###########  DEFINE DIRECTORIES #############
datadir=$XLSDATA/nomuraFiles/AEJdaily
logdir=$XLSDATA/nomuraFiles/AEJdaily/logs
SecureSpBCP_dir=$XLS/src/scripts/exports/Nomura/AEJDaily/
pipfile=CACE_AEJ_Party_$(date --date "${enddate}" +"%Y%m%d").dat 
cksumfile=CACE_AEJ_Party_$(date --date "${enddate}" +"%Y%m%d")_Checksum.dat
zipfile=CACE_AEJ_Party_$(date --date "${enddate}" +"%Y%m%d").zip
logfile=$logdir/CACE_AEJ_Party_$(date --date "${enddate}" +"%Y%m%d").log
#$(date +"%Y%m%d")

#move to the data dir
echo "Creating ${logdir}..."
mkdir -p $logdir
if [ $? -gt 0 ]
then
echo "Couldn't make directory ${logdir}"
fi


main (){

    echo "cd to ${datadir}"
    cd $datadir

    
    #get investigation data from database
    echo "Getting data from stored procedure getAEJDailyFileData..."
    ${SecureSpBCP_dir}/SecureSpBCP.exe --dbname=nomura --account=7266 --user=184595 --storedprocname=getAEJDailyFileData --params="@startDate=${startdate},@endDate=${enddate}" 1>$pipfile 2>>$logfile
    if [ $? -gt 0 ]
    then
    echo "Error getting data from stored procedure getAEJDailyFileData"
    exit 1
    fi

    filegentime=$(date +"%d/%m/%y %T")
    

    #### MAKE CHECK SUM FILE
    echo "Gathering Checksum Data..."
    businessdate=$(date --date "${enddate}" +"%d/%m/%y")
    wcout=$(wc -l $pipfile)
    wcoutarr=($wcout)
    #this is not the record count because Nomura wanted to include the header the real record count would be ((recordcount=${wcoutarr[0]-1}))
    ((recordcount=${wcoutarr[0]}))

    cksumout=$(cksum $pipfile)
    cksumoutlist=($cksumout)
    cksumval=${cksumoutlist[0]}
    filesize=${cksumoutlist[1]}

    md5sumout=$(md5sum $pipfile)
    md5sumoutlist=($md5sumout)
    md5sumvval=${md5sumoutlist[0]}
    
    echo "Creating Checksum File..."

    echo "BUSINESS_DATE=${businessdate}" > $cksumfile
    echo "RECORD_COUNT=$recordcount" >> $cksumfile
    echo "CHECKSUM_VALUE=$cksumval" >> $cksumfile
    echo "FILE_SIZE=$filesize" >> $cksumfile
    echo "MD5_SUM=$md5sumvval" >> $cksumfile
    echo "FILE_GENERATION_TIME=$filegentime" >> $cksumfile

    ####ZIP FILES (.dat)##############
    pkzipc -add $zipfile *.dat


    #####ENCRYPT ZIP FILE##############
    encryptionKey="C8995258"
    gpg -e -r $encryptionKey ${zipfile}
    if [ $? -ne 0 ]; then echo "Failed to encrypt files"; exit 1; fi;

    ##### UPLOAD ENCRYPTED FILES TO FTP ######
    ftpServer=`get_xls_registry_value nomuraftp Server 6`
    ftpUser=`get_xls_registry_value nomuraftp User 6`
    ftpPassword=`get_xls_registry_value nomuraftp Password 6`


    commandfile="command.ftp"
    echo "user ${ftpUser} ${ftpPassword}" > $commandfile
    echo "binary" >> $commandfile
    echo "mput *.gpg" >> $commandfile
    echo "quit" >> $commandfile
    ftp -i -n -s:$commandfile $ftpServer

    if [ $? -ne 0 ] 
    then
        echo "There was a problem uploading the files to ftp"
        exit 1
    fi

    ##### MAYBE CLEAN UP FILES, Could be done in separate script scheduled to run every so often

    ##remove the encrypted files
    rm *.dat
    rm *.zip
    rm *.gpg
    rm $commandfile


}

(main) &>> $logfile

if [ $? -ne 0 ]
then
blat ${logfile} -t administrators@alacra.com -s "Failed to export Nomura AEJDaily file"
fi



