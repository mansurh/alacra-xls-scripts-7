#!/bin/sh
today=`date +%a`
case $today in
Sun)  weekend=1;;
Mon)  weekend=1;;
esac
if [ $weekend = "1" ] ; then exit ; fi
localdir="/usr/ftp/sdc"
logdir="/usr/local/bin"
export localdir
export logdir
sdcfile="sdclog`date +%m%d`.log"
loop=1
echo "Start time (`date`)" > $logdir/sdclog.log

# we need to extract the date from DONE.ALL 
#just in case we did not deleted it from a previous update
if [  -e  $localdir/DONE.ALL ]
then
day=`ls -l $localdir/DONE.ALL | cut -c46-48`
current_day=`date +%-d`
if [ $current_day != $day  ]
then
  rm  -f $localdir/DONE.ALL
fi
fi
#end 

until [ -e $localdir/DONE.ALL ]
do
        sleep 15m
	while [ $loop -ge 48 ]
	do
          mail -s "SDC Copy STOPPED after 12 hours" administrators@xls.com <$logdir/sdclog.log
	  exit 
	done	
        loop=`echo "$loop + 1" |bc`
done
echo "Tar starts (`date`)" >> $logdir/sdclog.log
rm -f $localdir/sdc.tar
tar -cvzf $localdir/sdc.tar $localdir/*.d* 2>$logdir/tarlog.log
if [ $? -ne 0 ]
then
	echo "TAR on sdc.tar failed at `date`" >> $logdir/sdclog.log
	cat $logdir/tarlog.log >>$logdir/sdclog.log
	mail -s "SDC Compression log file" administrators@xls.com <$logdir/sdclog.log
        exit
fi
# mv $localdir/DONE.ALL $localdir/DONE.ALL.DONE
echo "Tar complete (`date`)" >> $logdir/sdclog.log
chown sdc:ftpaccount $localdir/sdc.tar
cp $logdir/sdcdone.txt $localdir
chown sdc:ftpaccount $localdir/sdcdone.txt
echo "FTP start (`date`)" >>$logdir/sdclog.log
ftp  -n ddl2.xls.com <$logdir/sdc.ftp 2>$logdir/error.log
if [ -s $logdir/error.log ]
then
	echo "Ftp on sdc.tar failed at `date`" >> $logdir/sdclog.log
	cat $logdir/error.log >> $logdir/sdclog.log
	mail -s "SDC Compression log file" administrators@xls.com <$logdir/sdclog.log
	exit 1
fi
ftp -n ddl2.xls.com <$logdir/sdcdone.ftp 2>$logdir/error.log
if [ -s $logdir/error.log ]
then
        echo "Ftp on sdcdone.txt failed at `date`" >> $logdir/sdclog.log
	cat $logdir/error.log >> $logdir/sdclog.log
        mail -s "SDC Compression log file" administrators@xls.com <$logdir/sdclog.log
	exit 1
fi
echo "FTP complete (`date`)" >> $logdir/sdclog.log
echo "End Time (`date`)" >> $logdir/sdclog.log
rm -f $localdir/DONE.ALL
rm -f $localdir/*.d*
cp $logdir/sdclog.log  /home/rsacwilk/$sdcfile
cp $logdir/sdclog.log /home/vomara/$sdcfile
mail -s "SDC Compression log file" isrequests@xls.com rsacwilk@xls.com <$logdir/sdclog.log
rm -f $logdir/*.log
exit 0
