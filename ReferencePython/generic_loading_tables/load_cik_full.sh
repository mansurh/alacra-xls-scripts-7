client_dir="cik"
destination="concordance"
stage="--stage 'prod'"
server="datatest4"
file_arg=""
max_dev="--max_dev 20"

if [ $# -gt 0 ]
then
  client_dir=$1 
fi
if [ $# -gt 1 ]
then
  destination=$2
fi
if [ $# -gt 2 ]
then
  server="--server $3"
fi
if [ $# -gt 3 ]
then
   file_arg="--file_arg $4"
fi
if [ $# -gt 4 ]
then
   max_dev="--max_dev $5"
fi

loaddir="$XLSDATA/${client_dir}"
logfile="${loaddir}/sh_log.log"
main()
{
 echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/cik_full.py"
 python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/cik_full.py

 echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/bcp_loader.py ${client_dir} ${destination} ${server} ${file_arg} ${max_dev}"
 python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/bcp_loader.py ${client_dir} ${destination} ${server} ${file_arg} ${max_dev}
}
main > ${logfile} 2>&1
if [ $? -ne 0 ] 
then
 
    	blat ${logfile} -t "administrators@alacra.com" -f "reference-data-feed@alacra.com" -s "CIK loading Delivery Failed "
    exit 1
fi
exit 0