import os
import time
from flask import Flask
from flask import request
from flask import render_template
from web_loader import Loader

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template("loader.html")

	
@app.route('/', methods=['POST'])
def my_form_post():
	client_dir = request.form['client-dir']
	origin = request.form['origin']
	destination = request.form['destination']
	os.popen("mkdir -p arg_files/" + client_dir)
	time.sleep(1)
	table_name = request.form['table-name']
	
	file_path = "arg_files/" + client_dir + "/create_" + table_name + ".txt"
	col_name = request.form.getlist('col-name')
	col_type = request.form.getlist('col-type')
	create_file(file_path, col_name, col_type)

	file_path = "arg_files/" + client_dir + "/load_" + table_name + ".txt"
	sql_commands_origin = request.form['sql_commands_origin']
	load_file(file_path, sql_commands_origin, 'origin')
	sql_commands_destination = request.form['sql_commands_destination']
	if len(sql_commands_destination) > 0:
		load_file(file_path, sql_commands_destination, 'destination')

	file_path = "arg_files/" + client_dir + "/procedure_" + table_name + ".txt"
	proc_name = request.form['proc_name']
	if len(proc_name) > 1:
		proc_file(file_path, proc_name)
	
	file_path = "arg_files/" + client_dir + "/parse_" + table_name + ".txt"
	parse_fields = request.form['parse_fields']
	if len(parse_fields) > 1:
		parse_file(file_path, parse_fields)

	
	loader = Loader(client_dir, origin, destination, table_name)
	data = loader.load()
	
	print(data)
	
	data_file = open ('templates/data.html','w')
	data_file.write(
				'''<!DOCTYPE html>
				<html lang="en">
				<head>
				<script src="static/loader.js"></script>
				</head>
				<body>
					<link rel="stylesheet" type="text/css" href="{{url_for('static',filename='css/loader.css')}}">
					<link rel="stylesheet" type="text/css" href="{{url_for('static',filename='css/alacrabook.css')}}">

					<div id="container-header">
						<div class="container-header-banner">
							<span>
								<img alt="Alacra Book" src="static/images/alacra-logo.png" class="booklogo">
							</span>
						</div>
					</div>
					<table class="data-table">''')
					
	for row in data[0:20]:
		data_file.write('''<tr class="tr-data-table">''')
		for col in row:
			data_file.write('''<td class="td-data-table">'''+col+'''</td>''')
		data_file.write('''</tr>''')
					
	data_file.write(
				'''</table>
				</body>
				</html>''')
	
	data_file.flush()
	
	return success_form_post(table_name, data) #render_template('success.html')
	
	
@app.route('/', methods=['POST'])
def success_form_post(table_name, data):
	render_template('success.html')
	#create_table = request.form['create-table']
	print("----------------here------------")
	return render_template('data.html',tname=table_name,data=data) #render_template('loader.html',success='true',tname=table_name)
	
	
def create_file(file_path, col_name, col_type):
	f = open(file_path, "w")
	columns = {}
	for index, col in enumerate(col_name):
		f.write(col + '|' + col_type[index] + '\r\n')
		columns[col] = col_type[index]
	f.flush()

def load_file(file_path, sql_commands, databaseType):
	f = open(file_path, "w")
	sql_commands = sql_commands.split("<><>")
	index = 0
	while index < len(sql_commands):
		sql_command = sql_commands[index]
		if (len(sql_command) > 0):
			f.write(databaseType + '|' + sql_command)
			if index < len(sql_commands)-1:
				f.write('\n<><>\n')
		index += 1
	f.flush()

def parse_file(file_path, parse_fields):
	f = open(file_path, "w")
	f.write(parse_fields)
	f.flush()
	
def proc_file(file_path, procName):
	f = open(file_path, "w")
	f.write('exec ' + procName)
	f.flush()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5001')