client_dir=""
destination="concordance"
stage="--stage 'prod'"
dest_server=""
origin_server=""
file_arg="--file_arg 'FINRA_wget'"
max_dev="--max_dev 20"

if [ $# -gt 0 ]
then
  client_dir=$1 
fi
if [ $# -gt 1 ]
then
  destination=$2
fi
if [ $# -gt 2 ]
then
  dest_server="--dest_server $3"
fi
if [ $# -gt 3 ]
then
  origin_server="--origin_server $4"
fi
if [ $# -gt 4 ]
then
   file_arg="--file_arg $5"
fi
if [ $# -gt 5 ]
then
   max_dev="--max_dev $6"
fi

loaddir="$XLSDATA/FINRA"
logfile="${loaddir}/sh_log.log"
main()
{
 echo "python3 ${XLS}/src/scripts/ReferencePython/wget_FINRA/get_files.py ${destination}"
 python3 ${XLS}/src/scripts/ReferencePython/wget_FINRA/get_files.py
 echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/bcp_loader.py ${client_dir} ${destination} ${stage} ${dest_server} ${file_arg} ${max_dev}"
 python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/bcp_loader.py ${client_dir} ${destination} ${stage} ${dest_server} ${file_arg} ${max_dev}
 echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${destination} ${destination} --table_name 'ipid_form5500' ${max_dev} ${stage} ${dest_server} ${origin_server}"
 python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${destination} ${destination} --tablename 'ipid_form5500' ${max_dev} ${stage} ${dest_server} ${origin_server}

}
main > ${logfile} 2>&1
exit 0
