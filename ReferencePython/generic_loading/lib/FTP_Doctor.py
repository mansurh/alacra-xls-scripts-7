import sys
import os
import pyodbc
from ftplib import FTP
import ftplib
import os.path
import subprocess as sproc
import gzip
import logging

class FTP_Doctor(object):
	regDir = None
	scriptdir = None
	loaddir = None
	file_name = None
	path = None
	is_zip = False
	suffix = None
	
	""" 
	Initializes the FTP_Doctor with a given source (client name)
	"""
	def __init__(self, regDir, path, file_name, db_conn):
		self.scriptdir = os.environ.get('XLS') + '/src/scripts/loading/' + regDir
		self.loaddir = os.environ.get('XLSDATA') + '/' + regDir
		self.path = path + '/'
		self.file_name = file_name
		cursor = db_conn.cursor()
		sql_command = 'SELECT name FROM ipid_unzip'
		untrimmed_zip = cursor.execute(sql_command).fetchall()
		zip = []
		for ele in untrimmed_zip:
			zip.append(ele[0])
		self.regDir = regDir
		index = file_name.rfind('.') # error check for no '.'
		if file_name[index:] in zip:
			print(file_name[index:])
			self.is_zip = True
			self.suffix = file_name[index:]
			#self.unzip_file(file_name[index:], db_conn)
		#initialize logging file name
		logging.basicConfig(filename='logger_bad.log', level=logging.DEBUG, 
		format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p: ')
		
	"""
	Opens a connection to the given ftp host
	Param: host - the url of the host you will connect to
	"""
	def open_connection(self, host, user, pwd):
		ftp_conn = FTP(host, user, pwd)
		return ftp_conn
	
	def retrieve_data(self, ftp_conn, choice):
		if choice == 'update':
			index = self.file_name.rfind('.')
			suffix = self.file_name[index:] # error check for no '.'
			new_file_name = self.file_name[:index] + '_new' + suffix
			complete_path = os.path.join(self.loaddir, new_file_name)
		else:
			complete_path = os.path.join(self.loaddir, self.file_name)
		print(complete_path)
		try:
			file = open(complete_path, 'wb') 
		except FileNotFoundError:
			logging.error('File not found error: Invalid path or FTP file name.')
			print('File not found error: Invalid path or FTP file name.')
			sys.exit()
		print('path: ' + self.path + self.file_name)
		try:
			ftp_conn.cwd(self.path)
			return ftp_conn.retrbinary('RETR %s' % self.file_name, file.write)
		except ftplib.error_perm:
			logging.error("ftplib.error_perm: FTP path and/or file not found.")
			print("ftplib.error_perm: FTP path and/or file not found. Exiting system.")
			sys.exit()
			
	def unzip_file(self, suffix, db_conn):
		if '.tar.gz' in self.file_name:
			suffix = '.tar.gz' #ensure the suffix isn't '.gz' for a '.tar.gz'
		rmdir_command = 'rm -rf ' + self.loaddir + '/zip_files'
		mkdir_command = 'mkdir -p ' + self.loaddir + '/zip_files'
		print('rmcommand---------------------')
		print(os.popen(rmdir_command).read())
		print(os.popen(mkdir_command).read())
		cursor = db_conn.cursor()
		sql_command = "select value from ipid_unzip where name='" + suffix + "'"
		try:
			unix_command = cursor.execute(sql_command).fetchone()[0]
			unix_command = unix_command.replace('$filename', self.file_name)
			unix_command = unix_command.replace('$loaddir', self.loaddir)
			unix_command = unix_command.replace('$scriptdir', self.scriptdir)
			print(unix_command)
			print(os.popen(unix_command).read())
		except TypeError:
			logging.error('Zip type: "' + suffix + '" is not currently supported, add it to the ipid_unzip table.')
			print('Zip type: "' + suffix + '" is not currently supported, add it to the ipid_unzip table. Exiting system.')
			sys.exit()
			
	